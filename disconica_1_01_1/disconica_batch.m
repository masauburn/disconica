function disconica_batch()

%
path_name = fullfile(pwd,'SOFT4')
formatOut = 'mm_dd_yy';
addpath(path_name)
%[pathstr,name,ext] = fileparts(soft)

formatOut = 'mm_dd_yy';
log_fn = ['disconica_log_' datestr(now,formatOut) '.log'];
log_loc = fullfile(path_name, 'logs');
data_loc = fullfile(path_name, 'data');

if exist(log_loc, 'dir') ~= 7
    mkdir(log_loc);
end

log_fn = fullfile(log_loc, log_fn);

if exist(log_fn, 'file') == 2
    open = 'a';
else
    open = 'w';
end

%
log = sprintf('%s %s', datestr(now), 'DisConICA Batch Started');
log = sprintf('%s\n%s %s', log, datestr(now), 'Setting up initialization parameters');

fileID = fopen(log_fn, open);
fprintf(fileID, log);
fclose(fileID);

try

    pwd
    param_fn = 'parameters.mat';
    param_loc = fullfile(data_loc, param_fn);
    
    if exist(param_loc, 'file') == 2
        
        load(param_loc);
    
        % Start Create FSL Environment
        %test_param = parameters;
        
        f_string = strsplit(parameters.fsl_string, ';');

        error = 0;

        log = sprintf('%s\n%s %s', log, datestr(now), 'Setting up FSL initialization');

        if (~isempty(f_string))
            for i = 1:length(f_string)

                log = sprintf('%s\n%s %s %s', log, datestr(now), 'FSL Instruction:', char(f_string(i)));
                status = system(char(f_string(i)));
                log = sprintf('%s\n%s %s %s', log, datestr(now), 'Completed FSL Instruction:', status);

            end
        end


        % End Create FSL Environment
        parameters.path_name = path_name;

        % Call PrepEnvironment

        if (error == 0)

            log = sprintf('%s\n%s %s', log, datestr(now), 'Calling PrepEnvironment') ;  
            [error, groups] = PrepEnvironment(parameters);
            log = sprintf('%s\n%s %s %s', log, datestr(now), 'In DisConICA, PrepEnvironment Completed, Error = ', error);

        end
        %

        if (error == 0)

            log = sprintf('%s\n%s %s', log, datestr(now), 'Calling PrepRunDPARSF');  
            [error, groups] = PrepRunDPARSF(parameters, groups);
            log = sprintf('%s\n%s %s %s', log, datestr(now), 'In DisConICA, PrepRunDPARSF Completed, Error = ', error);

        end

        if (error == 0)

            log = sprintf('%s\n%s %s', log, datestr(now), 'Calling PrepRunMELODIC');   
            [error, groups] = PrepRunMELODIC(parameters, groups);
            log = sprintf('%s\n%s %s %s', log, datestr(now), 'In DisConICA, PrepRunMELODIC Completed, Error = ', error);

        end

        if (error == 0)

            log = sprintf('%s\n%s %s', log, datestr(now), 'Calling PrepRungRAICAR');   
            [error, groups] = PrepRungRAICAR(parameters, groups);
            log = sprintf('%s\n%s %s %s', log, datestr(now), 'In DisConICA, PrepRungRAICAR Completed, Error = ', error);

        end
        
        %parameters.folder_name
        
        if (error == 0)

            log = sprintf('%s\n%s %s', log, datestr(now), 'Calling SetupClustering');   
            error = SetupClustering(parameters);
            log = sprintf('%s\n%s %s %s', log, datestr(now), 'In DisConICA, SetupClustering Completed, Error = ', error);

        end
        
        

        if (error == 1)

            fileID = fopen(log_fn, open);
            fprintf(fileID, log);
            fclose(fileID);

        end
        
    else
        
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'File Not Found: ', param_loc) ;
        fileID = fopen(log_fn, open);
        fprintf(fileID, log);
        fclose(fileID);
        
    end
    
catch e
    
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'Main Error, Identifier: ', e.identifier);
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'Main Error, Message: ', e.message);
    
    fileID = fopen(log_fn, open);
    fprintf(fileID, log);
    fclose(fileID);
    
end

