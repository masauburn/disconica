function [ r, p ] = GetCorrCoeff(path_1, path_2, ax, ay, az)

a_1 = load_nii(path_1)
x_1 = squeeze(a_1.img(:,:,:));
x_1 = reshape (x_1,1, ax*ay*az);

a_2 = load_nii(path_2)
x_2 = squeeze(a_2.img(:,:,:));
x_2 = reshape (x_2,1,ax*ay*az);

[R, P] = corrcoef(x_1, x_2)
r = det(R);
p = det(P);


end

