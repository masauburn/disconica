function Test_Results(res_str, pathstr)

formatOut = 'mm_dd_yy';
soft = mfilename('fullpath');
[pathstr,name,ext] = fileparts(soft);

formatOut = 'mm_dd_yy';
res_fn = ['disconica_results_' datestr(now,formatOut) '.html'];
res_loc = fullfile(pathstr, 'results');
res_fn = fullfile(res_loc, res_fn);

log_loc = fullfile(pathstr, 'logs');
data_loc = fullfile(pathstr, 'data');


res = sprintf('%s ', '<!DOCTYPE html><html><body bgcolor="#151B54"><div id = "header"><a href="www.auburn.edu"><img src="images\\logo.png" alt = "Auburn University MRI Center"></a><a href="http://www.eng.auburn.edu/research/centers/mri/index.html"><h2 style="font-family:verdana;color:white">Magnetic Resonance Imaging (MRI) Research Center</h2></a><h2 style="font-family:verdana;color:white">DisConICA v 1.0.1</h2></div>');
res = sprintf('%s\n%s %s %s', res, '<h3 style="font-family:verdana;color:#ffcc66"> Date/ Time: ', datetime('now'), '</h3>');
res = sprintf('%s\n%s', res, '<h3 style="font-family:verdana;color:#ffcc66"> ****Experimentation Results****</h3><br /><br />');
res = sprintf('%s\n<p style="font-family:verdana;color:#ffcc66">%s%s', res, res_str, '</p><br /><br /><hr>');
res = sprintf('%s\n%s', res, '</body></html>');


fileID = fopen(res_fn, 'wt');
fprintf(fileID, res);
fclose(fileID);

end