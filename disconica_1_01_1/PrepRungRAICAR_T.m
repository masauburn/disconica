function error = PrepRungRAICAR_T(parameters)
% get average mask script
error = 0;

soft = mfilename('fullpath');
[pathstr,name,ext] = fileparts(soft);

if (~isempty(parameters.path_name))
    pathstr = parameters.path_name
end
%pathstr = '/gpfs01/home/mas0047/SOFT4'
addpath(pathstr)
formatOut = 'mm_dd_yy';
log_fn = ['disconica_log_' datestr(now,formatOut) '.log'];
log_loc = fullfile(pathstr, 'logs');

if exist(log_loc, 'dir') ~= 7
    mkdir(log_loc);
end

log_fn = fullfile(log_loc, log_fn);

if exist(log_fn, 'file') == 2
    open = 'a';
else
    open = 'w';
end

%
log = sprintf('%s %s', datestr(now), 'PrepRungRAICAR Started');
log = sprintf('%s\n%s %s', log, datestr(now), 'Setting up initialization parameters');

fileID = fopen(log_fn, open);
fprintf(fileID, log);
fclose(fileID);

%
folder_name = parameters.folder_name;%'C:\Users\Saidah\Desktop\TestData4';
        %put for loop here
        
        destName = folder_name; %varargin{1}; %main directory with groups
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'destName: ', destName);
        
        files = dir(destName);
        dirFlags = [files.isdir];
        subFolders = files(dirFlags);
        folders = {};
        
        %We are getting all group folders here
        log = sprintf('%s\n%s %s', log, datestr(now), 'Getting group folders: ');
        i = 1;
        for k = 1 : length(subFolders)
        
        if (strcmp(subFolders(k).name, '.') == 0) && (strcmp(subFolders(k).name, '..') == 0)
            f1 = subFolders(k).name;
            folders = [folders f1];
            groups(i).name = subFolders(k).name;
            i=i+1;
            
        end
        
        end
        
        
        numGroups = length(groups);
        for i = 1:numGroups
            groups(i).path = [folder_name '/' groups(i).name '/' 'Analysis'];

        end
        for i = 1:numGroups
            groups(i).MELODIC_path = [folder_name '/' groups(i).name '/' 'Analysis' '/' 'Melodic'];

        end

%

try
    
    NIFTI_PATH = sprintf('%s/%s', pathstr, 'NIfTI');
    GR_PATH = sprintf('%s/%s', pathstr, 'gRAICAR');
    GRCORE_PATH = fullfile(pathstr, 'gRAICAR', 'core');
    GRFASTICA_PATH = fullfile(pathstr, 'gRAICAR', 'fastICA');
    GRRAICAR_PATH = fullfile(pathstr, 'gRAICAR', 'RAICAR_utl');
    GRSTRINGS_PATH = fullfile(pathstr, 'gRAICAR', 'strings');
    
     % add path to gRAICAR
    addpath(GR_PATH);
    % add path to NIFTI Software
    addpath(NIFTI_PATH);
    addpath(GRCORE_PATH);
    addpath(GRFASTICA_PATH);
    addpath(GRRAICAR_PATH);
    addpath(GRSTRINGS_PATH);
    
    numGroups = numel(groups);
    
    for i = 1:numGroups

        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Group: ', groups(i).name);
        log = sprintf('%s\n%s %s', log, datestr(now), 'Creating gR path: ');
        mkdir(groups(i).path, 'gR');
        groups(i).gR_path = fullfile(groups(i).path, 'gR');
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'gR path created: ', groups(i).gR_path);

        strAvMask = '#!/bin/sh \n fslmaths ';
        strSubList = '';

        log = sprintf('%s\n%s %s', log, datestr(now), 'Copying from Melodic directory to gR directory: ');
        command = sprintf('cp -r %s/*/ %s', groups(i).MELODIC_path, groups(i).gR_path);
        [status, cmdout] = system(command);
        log = sprintf('%s\n%s %s %s %s', log, datestr(now), 'copied: ', status, ' ', cmdout);
        %And this filters out the parent and current directory '.' and '..'
        e = dir(groups(i).gR_path);
        isub = [e(:).isdir]; %# returns logical vector
        e = {e(isub).name}';
        e(ismember(e,{'.','..'})) = [];

        numFolds = length(e);

        numFiles = 0;
        destFolder = groups(i).MELODIC_path;

        tic
        for j = 1:numFolds

                foldName = e{j};

                if j == 1
                    strSubList = [foldName '\n'];
                else
                    strSubList = [strSubList foldName '\n'];
                end

                foldName = fullfile(groups(i).gR_path, foldName);
                maskFilegz = fullfile(foldName, 'filtered_func_data.ica', 'mask.nii.gz');
                %gunzip(maskFilegz);
                maskFile = fullfile(foldName, 'filtered_func_data.ica', 'mask.nii.gz');
                %maskFile = strrep(maskFile, '\', '\\'); 

                if j == 1
                    strAvMask = [strAvMask ' ' maskFile];
                else
                    strAvMask = [strAvMask ' -add ' maskFile];
                end

        end


        strAvMask = [strAvMask ' -div ' num2str(numFolds) ' ' groups(i).gR_path '/mask'];

        maskLoc = fullfile(groups(i).gR_path, 'createMask.sh');
        subListLoc = fullfile(groups(i).gR_path, 'subjects.list');

        fid = fopen(maskLoc, 'w');
        fprintf(fid, strAvMask);
        fclose(fid);

        fid = fopen(subListLoc, 'w');
        fprintf(fid, strSubList);
        fclose(fid);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'subject list created: ', subListLoc);
    end
    
    log = sprintf('%s\n%s %s', log, datestr(now), 'Permissions setting Av mask script: ');
    
    for i = 1:numGroups
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Group: ', groups(i).name);
        command = sprintf('chmod a+x %s/%s', groups(i).gR_path, 'createMask.sh');
        [status, cmdout] = system(command);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Permissions set: ', status, ' ', cmdout);


    end
    
    log = sprintf('%s\n%s %s', log, datestr(now), 'Running av mask script:' )
    
    for i = 1:numGroups
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Group: ', groups(i).name) ;
        command = sprintf('%s/createMask.sh', groups(i).gR_path);
        [status, cmdout] = system(command);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Av mask ran: ', status, ' ', cmdout);  
    end

    log = sprintf('%s\n%s %s', log, datestr(now), 'Setting gR paths: ');
    
    for i = 1:numGroups
        
       gRIP(i).workdir = groups(i).gR_path;
       gRIP(i).core = GRCORE_PATH;
       gRIP(i).fastICA = GRFASTICA_PATH;
       gRIP(i).raicar = GRRAICAR_PATH;
       gRIP(i).strings = GRSTRINGS_PATH;


    end
    
    log = sprintf('%s\n%s %s', log, datestr(now), 'gR paths set: '); 

    log = sprintf('%s\n%s %s', log, datestr(now), 'Running gRAICAR: '); 

    tic
    
    parfor index = 1:numGroups

        [Error] = gRAICAR_Custom(gRIP(index));

    end
        
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'Elapsed Time for gRAICAR', toc);
    
    log = sprintf('%s\n%s %s', log, datestr(now), 'PrepRungRAICAR Completed ');  
    log = strrep(log, '\', '\\');
    fileID = fopen(log_fn, open);
    fprintf(fileID, log);
    fclose(fileID);
    
catch e
    
    error = 1;
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'PrepRungRAICAR Error, Identifier: ', e.identifier); 
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'PrepRungRAICAR Error, Message: ', e.message); 
    log = strrep(log, '\', '\\');
    fileID = fopen(log_fn, open);
    fprintf(fileID, log);
    fclose(fileID);
    return;

    
end