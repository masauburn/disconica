function [error, opp_vec] = SetupOpp_Vec(resultFile, melodicFiles, index, pathstr)


formatOut = 'mm_dd_yy';
log_fn = ['disconica_log_' datestr(now,formatOut) '.log'];
% soft = mfilename('fullpath');
% [pathstr,name,ext] = fileparts(soft);
NIFTI_PATH = sprintf('%s/%s', pathstr, 'NIfTI');
addpath(NIFTI_PATH);
log_loc = fullfile(pathstr, 'logs');

if exist(log_loc, 'dir') ~= 7
    mkdir(log_loc);
end

log_fn = fullfile(log_loc, log_fn);

if exist(log_fn, 'file') == 2
    open = 'w';
else
    open = 'a';
end

%
log = sprintf('%s %s', datestr(now), 'Setup Opp_vec Started');
log = sprintf('%s\n%s %s', log, datestr(now), 'Setting up initialization parameters');

fileID = fopen(log_fn, open);
fprintf(fileID, log);
fclose(fileID);
error = 0;
%opp_vec = [];

try
    
    %
    load(resultFile);

    s = size(obj.result.foundComp(1,:));

    ICIndex = {};               %# Read the first element (1,1)

    for i1 = 1: s(2)               %# Loop from subject 1 thru s(1)
        ICIndex = [ICIndex obj.result.foundComp(index, i1)];       %# Add the element to ICIndex
    end

    for i2 = 1: length(melodicFiles)
        % files have been added because they exist

        a = load_nii(melodicFiles{1,i2});
        ax=a.hdr.dime.dim(2);
        ay=a.hdr.dime.dim(3);
        az=a.hdr.dime.dim(4);
        x = squeeze(a.img(:,:,:,ICIndex{i2}));

        if i2 == 1

            x_vec=reshape(x,1,ax*ay*az);

        else

            y_vec=reshape(x,1,ax*ay*az);
            x_vec = [x_vec; y_vec];

        end



    end
    
    opp_vec = x_vec;
%                 

        
    log = sprintf('%s\n%s %s', log, datestr(now), 'Ending Set Up Opp Vec');
    fileID = fopen(log_fn, open);
    fprintf(fileID, log);
    fclose(fileID);
    
catch e
    
    error = 1;
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'Main Error, Identifier: ', e.identifier);
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'Main Error, Message: ', e.message);
    log = strrep(log, '\', '\\');
    log = sprintf('%s\n%s %s', log, datestr(now), 'Set up Opp Vec did not complete successfully');
    fileID = fopen(log_fn, open);
    fprintf(fileID, log);
    fclose(fileID);
    
end