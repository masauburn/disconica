function [error, groups] = PrepRunMELODIC(parameters, groups)


error = 0;

soft = mfilename('fullpath');
[pathstr,name,ext] = fileparts(soft);

if (~isempty(parameters.path_name))
    pathstr = parameters.path_name
end
addpath(pathstr)
formatOut = 'mm_dd_yy';
log_fn = ['disconica_log_' datestr(now,formatOut) '.log'];
log_loc = fullfile(pathstr, 'logs');

if exist(log_loc, 'dir') ~= 7
    mkdir(log_loc);
end

log_fn = fullfile(log_loc, log_fn);

if exist(log_fn, 'file') == 2
    open = 'a';
else
    open = 'w';
end

%
log = sprintf('%s %s', datestr(now), 'PrepgMedica Started')
log = sprintf('%s\n%s %s', log, datestr(now), 'Setting up initialization parameters')

fileID = fopen(log_fn, open);
fprintf(fileID, log);
fclose(fileID);

try
    
    
    
    TEMP_PATH = sprintf('%s/%s', pathstr, 'template.fsf');
    STD_BRAIN_PATH = sprintf('%s/%s', pathstr, 'standard/MNI152_T1_2mm_brain');
    
    f_string = strsplit(parameters.fsl_string, ';');

        error = 0;

        log = sprintf('%s\n%s %s', log, datestr(now), 'Setting up FSL initialization');

        if (~isempty(f_string))
            for i = 1:length(f_string)

                log = sprintf('%s\n%s %s %s', log, datestr(now), 'FSL Instruction:', char(f_string(i)));
                status = system(char(f_string(i)));
                log = sprintf('%s\n%s %s %s', log, datestr(now), 'Completed FSL Instruction:', status);

            end
        end
    

    log = sprintf('%s\n%s %s', log, datestr(now), 'Creating Melodic Path: ');
    
    numGroups = numel(groups);
    
    for i = 1:numGroups

      log = sprintf('%s\n%s %s %s', log, datestr(now), 'Group: ', i);
      groups(i).DPARSF_result_path = fullfile(groups(i).path, 'FunImgARWSDF');
      log = sprintf('%s\n%s %s %s', log, datestr(now), 'DPARSF_result_path: ', groups(i).DPARSF_result_path);
      mkdir(groups(i).path, 'Melodic');
      groups(i).MELODIC_path = fullfile(groups(i).path, 'Melodic');
      log = sprintf('%s\n%s %s %s', log, datestr(now), 'Melodic path: ', groups(i).MELODIC_path);  


    end

    % % Read post DPARSF folder
    % 
    log = sprintf('%s\n%s %s', log, datestr(now), 'Reading post-dparsf folder: ');
    for i = 1:numGroups

        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Group: ', i);
        e = dir(groups(i).DPARSF_result_path);
        isub = [e(:).isdir]; %# returns logical vector
        e = {e(isub).name}';
        e(ismember(e,{'.','..'})) = [];

        numFolds = length(e);
        numFiles = 0;
        destFolder = groups(i).MELODIC_path;

        tic
            for j = 1:numFolds

                niiName = e{j};
                niiFile = sprintf('%s.nii', num2str(niiName));
                dest = fullfile(destFolder, niiFile);
                source = fullfile(groups(i).DPARSF_result_path, e{j}, 'Filtered_4DVolume.nii');
                log = sprintf('%s\n%s %s %s', log, datestr(now), 'Source: ', source);
                log = sprintf('%s\n%s %s %s', log, datestr(now), 'Dest: ', dest);
                if exist (dest, 'file') == 0
                    copyfile(source, dest);
                end
                log = sprintf('%s\n%s %s', log, datestr(now), 'Copy completed ');


    %      
                 if (i == 1)

                        xnii = load_nii(source);
                        ax = xnii.hdr.dime.dim(2);
                        ay = xnii.hdr.dime.dim(3);
                        az = xnii.hdr.dime.dim(4);
                        log = sprintf('%s\n%s %s %s', log, datestr(now), 'ax: ', ax);
                        log = sprintf('%s\n%s %s %s', log, datestr(now), 'ay: ', ay);
                        log = sprintf('%s\n%s %s %s', log, datestr(now), 'az: ', az);

                 end

            end

    end


    % Create feat file and adjust
    log = sprintf('%s\n%s %s', log, datestr(now), 'Creating Feat file and adjusting: ');
    % ax=91;
    % ay=109;
    % az=91;
    % 

    for i = 1:numGroups

        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Group: ', groups(i).name);

        rowTR = sprintf('# TR(s)\nset fmri(tr)');
        rownewTR = sprintf('%s %s', rowTR, num2str(parameters.TR));

        rowVol = sprintf('# Total volumes\nset fmri(npts)');
        rownewVol = sprintf('%s %s', rowVol, num2str(groups(i).timepoints)); %groups(1).timepoints);

        rowTotVox = sprintf('# Total voxels\nset fmri(totalVoxels)') ;
        totalVoxel = ax * ay * az * groups(i).timepoints;
        rownewTotVox = sprintf('%s %s', rowTotVox, num2str(totalVoxel));

        rowStdBrain = 'fmri(regstandard)'; % std brain path
        rownewStdBrain = sprintf('%s "%s"', rowStdBrain, STD_BRAIN_PATH);

        rowSubs = 'set fmri(multiple)'; % num subjects

        %     
         rowa = '# 4D AVW data or FEAT directory ';
         rowb = 'set feat_files';
    %     
         str_lookup = '# 4D AVW data or FEAT directory';
    %     
         feat_file_in = TEMP_PATH;
         feat_file_out = fullfile(groups(i).MELODIC_path, 'design.fsf');
    % 
         newstr = '';
         pattern = fullfile(groups(i).MELODIC_path,'*.nii');
         files = dir(pattern);   %# list all *.xyz files
         files = {files.name}';                      %'# file names

         rownewSubs = sprintf('%s %s', rowSubs, num2str(numel(files)));

         data = cell(numel(files),1);                %# store file contents
    %     
         for j=1:numel(files)
             fname = fullfile(groups(i).MELODIC_path,files{j});     %# full path to file
             row1 = sprintf('%s (%s)\n', rowa, num2str(j));
             fname = strrep (fname, '\', '/');
             row2 = sprintf('%s(%s) "%s"\n', rowb, num2str(j), fname); 
             newstr = sprintf('%s %s %s', newstr, row1, row2);

        end

        newstr = sprintf('%s\n', newstr);

        fid  = fopen(feat_file_in,'r');


        f=fileread(feat_file_in);%,'*char')';
        fclose(fid);

        indices = strfind(f, rowTR)
        indices = strfind(f, rowVol)
        indices = strfind(f, rowTotVox)

        f = strrep(f, rowTR, rownewTR);
        f = strrep(f, rowVol, rownewVol);
        f = strrep(f, rowTotVox, rownewTotVox);
        f = strrep(f, rowStdBrain, rownewStdBrain);
        f = strrep(f, rowSubs, rownewSubs);
        f = strrep(f,str_lookup,newstr);
    %     
        fid  = fopen(feat_file_out,'w+');
        fprintf(fid, '%s', f);
        fclose(fid);

        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Feat file created: ', feat_file_out);  

        %command = 'feat design.fsf';
        %cd(destName);
        %[status,cmdout] = system(command)
        %status
        %run feat command


    end


    %   
    tic;
    log = sprintf('%s\n%s %s', log, datestr(now), 'Running FSL MELODIC: ');

    parfor index = 1:numGroups
        command = sprintf('feat %s/%s',char(groups(index).MELODIC_path), 'design.fsf');
        %cd(destName);
        [status,cmdout] = system(command);
%                 status
%                 cmdout
          %run feat command
    end

    log = sprintf('%s\n%s %s %s', log, datestr(now), 'Elapsed Time for FSL: ', toc);

    log = sprintf('%s\n%s %s', log, datestr(now), 'PrepRunMELODIC Completed ');  
    log = strrep(log, '\', '\\');
    fileID = fopen(log_fn, open);
    fprintf(fileID, log);
    fclose(fileID);
    
catch e
    
    error = 1;
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'PrepRunMELODIC Error, Identifier: ', e.identifier); 
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'PrepRunMELODIC Error, Message: ', e.message); 
    log = strrep(log, '\', '\\');
    fileID = fopen(log_fn, open);
    fprintf(fileID, log);
    fclose(fileID);
    return;
    
end
        