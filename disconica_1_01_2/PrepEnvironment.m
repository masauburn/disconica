function [error, groups] = PrepEnvironment(parameters)

error = 0;

soft = mfilename('fullpath');
[pathstr,name,ext] = fileparts(soft);

if (~isempty(parameters.path_name))
    pathstr = parameters.path_name
end

addpath(pathstr)

formatOut = 'mm_dd_yy';
log_fn = ['disconica_log_' datestr(now,formatOut) '.log'];
log_loc = fullfile(pathstr, 'logs');

if exist(log_loc, 'dir') ~= 7
    mkdir(log_loc);
end

log_fn = fullfile(log_loc, log_fn);

if exist(log_fn, 'file') == 2
    open = 'a';
else
    open = 'w';
end

%
log = sprintf('%s %s', datestr(now), 'PrepEnvironment Started')
log = sprintf('%s\n%s %s', log, datestr(now), 'Setting up initialization parameters')

fileID = fopen(log_fn, open);
fprintf(fileID, log);
fclose(fileID);

try

    NIFTI_PATH = sprintf('%s/%s', pathstr, 'NIfTI')
    SPM_PATH = sprintf('%s/%s', pathstr, 'spm8')
    REST_PATH = sprintf('%s/%s', pathstr, 'REST18')
    DPARSF_PATH = sprintf('%s/%s/%s', pathstr, 'DPABI','DPARSF')
    GR_PATH = sprintf('%s/%s', pathstr, 'gRAICAR')
    TEMP_PATH = sprintf('%s/%s', pathstr, 'template.fsf')
    STD_BRAIN_PATH = sprintf('%s/%s', pathstr, 'standard/MNI152_T1_2mm_brain')
    DCM_PATH = sprintf('%s/%s', DPARSF_PATH, 'dcm2nii')
    DPABI_PATH = sprintf('%s/%s', pathstr, 'DPABI')
    SOFT_PATH = pathstr

    
    % Log
    %log = sprintf('%s\n%s %s %s', log, datestr(now), 'soft: ', soft);
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'NIFTI_PATH: ', NIFTI_PATH);
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'SPM_PATH: ', SPM_PATH);
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'REST_PATH: ', REST_PATH);
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'DPARSF_PATH: ', DPARSF_PATH);
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'GR_PATH: ', GR_PATH);
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'TEMP_PATH: ', TEMP_PATH);
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'STD_BRAIN_PATH: ', STD_BRAIN_PATH);
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'DCM_PATH: ', DCM_PATH);
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'DPABI_PATH: ', DPABI_PATH);
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'SOFT_PATH: ', SOFT_PATH);
    

    % 

    log = sprintf('%s\n%s %s', log, datestr(now), 'Adding Paths');

   
    % add path to gRAICAR
    addpath(GR_PATH);
    % add path to NIFTI Software
    addpath(NIFTI_PATH);
    % % add path to SPM
    addpath(SPM_PATH);
    % % add path to REST
    addpath(REST_PATH);
    % % add path to DPARSF
    addpath(DPARSF_PATH);
    %addpath('C:\Users\Saidah\Desktop\PTSDStuff\DPABI\DPABI_V1.3_150710\DPARSF_V3.2_150710'); 
    addpath(DPABI_PATH); 
    addpath(fullfile(DPABI_PATH, 'Subfunctions'));
    addpath(fullfile(DPARSF_PATH, 'Subfunctions'));
    addpath(fullfile(DPARSF_PATH, 'Jobmats'));

    folder_name = parameters.folder_name;
    TR = parameters.TR;

    log = sprintf('%s\n%s %s %s', log, datestr(now), 'folder_name: ', folder_name);
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'TR: ', TR);


    %Remove 'temp' directory from any previous analysis




    temp_dir = fullfile(folder_name, 'temp');

    log = sprintf('%s\n%s %s %s', log, datestr(now), 'temp_dir: ', temp_dir);

    if (isequal(exist(temp_dir, 'dir'),7)) % 7 = directory. Restart Analysis, Remove previous work

        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Previous Work Exists: ', temp_dir);

        cmd_str = sprintf('rm -rf %s', temp_dir); 
        [status, result] = system(cmd_str);

        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Previous Work Removed: ', result);


    end



    d = dir(folder_name);
    % d = dir(pathFolder);
    isub = [d(:).isdir]; %# returns logical vector
    nameFolds = {d(isub).name}';
    nameFolds(ismember(nameFolds,{'.','..'})) = [];

    numFolds = length(nameFolds);
    numFiles = 0;
    % set up groups
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'Setting Up Groups, Numfolds: ', numFolds);


    for i = 1:numFolds

        groups(i).name = nameFolds{i};
        groups(i).path = [folder_name '/' groups(i).name];

        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Setting Up Groups, name: ', groups(i).name);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Setting Up Groups, path: ', groups(i).path);
        
    end
    
    numGroups = numel(groups);
    
    % Remove Analysis Directory - If Present
    
    for i = 1:numGroups

        analysis_dir = fullfile(groups(i).path, 'Analysis');

        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Checking Prior Analysis Directory: ',  analysis_dir);

        if (isequal(exist(analysis_dir, 'dir'),7)) % 7 = directory. Restart Analysis, Remove previous work
            log = sprintf('%s\n%s %s %s', log, datestr(now), 'Analysis Directory Found: ',  analysis_dir);
            cmd_str = sprintf('rm -rf %s', analysis_dir); 
            [status, result] = system(cmd_str)
            log = sprintf('%s\n%s %s %s %s', log, datestr(now), 'Analysis Directory Remove Status: ', status, result);

        else
           log = sprintf('%s\n%s %s %s', log, datestr(now), 'Prior Analysis Directory not found: ',  analysis_dir);
        end
        %waitbar(i/numGroups,h,sprintf('percentage = %2.2f',i/numGroups*100))
    end
    
    % Get Subjects
    
    for i = 1:numGroups

        e = dir(groups(i).path);
        isub = [e(:).isdir]; %# returns logical vector
        e = {e(isub).name}';
        e(ismember(e,{'.','..'})) = [];

         numSubFolds = length(e);

         for j = 1:numSubFolds

             groups(i).subject(j).name = e{j};
             groups(i).subject(j).path = [groups(i).path '/' groups(i).subject(j).name];

             log = sprintf('%s\n%s %s %s', log, datestr(now), 'Setting Up Subj , name: ',  groups(i).subject(j).name);
             log = sprintf('%s\n%s %s %s', log, datestr(now), 'Setting Up Subj , name: ', groups(i).subject(j).path);

         end
    %waitbar(i/numFolds,h,sprintf('percentage = %2.2f',i/numFolds*100))

    end

    

    



    for i = 1:numGroups
        
        groups(i).numSubjects = numel(groups(i).subject); % Test for subject # equality

    end


    % Checking the number of time points at group level 
    flag_FILE_TYPE = 0;

    errI = 0;

    for i = 1:numGroups
    %     
        for j = 1:groups(i).numSubjects

            dcm = dir([groups(i).subject(j).path, '/*.dcm']); 
            groups(i).subject(j).dcm = length(dcm(not([dcm.isdir])));

            img = dir([groups(i).subject(j).path, '/*.img']);
            groups(i).subject(j).img = length(img(not([img.isdir])));

            log = sprintf('%s\n%s %s %s', log, datestr(now), 'Looking for image type - dcm: ',  groups(i).subject(j).path);
            log = sprintf('%s\n%s %s %s', log, datestr(now), 'Looking for image type - dcm: ',  groups(i).subject(j).dcm);

            log = sprintf('%s\n%s %s %s', log, datestr(now), 'Looking for image type - img: ',  groups(i).subject(j).path);
            log = sprintf('%s\n%s %s %s', log, datestr(now), 'Looking for image type - img: ',  groups(i).subject(j).img);
            % Add more image types

            if (groups(i).subject(j).dcm > 0 && groups(i).subject(j).img > 0) %Heterogeneous mixture of image types

                log = sprintf('%s\n%s %s %s %s', log, datestr(now), 'Mixed files: ',  groups(i).subject(j).dcm, groups(i).subject(j).img);
                flag_FILE_TYPE = 1; 
                return;

            elseif (groups(i).subject(j).img > 0)
                groups(i).subject(j).imgType = 0;   %Img/Hdr
                ImgType = 0;
                log = sprintf('%s\n%s %s %s', log, datestr(now), 'Setting image type - img: ',  groups(i).subject(j).imgType);


            elseif (groups(i).subject(j).dcm > 0)
                groups(i).subject(j).imgType = 1;   %dcm
                ImgType = 1;
                log = sprintf('%s\n%s %s %s', log, datestr(now), 'Setting image type - dcm: ',  groups(i).subject(j).imgType);


            end
        end    
    end

    %

    for i = 1:numGroups
    %     
        for j = 1:groups(i).numSubjects
            if (j == 1)
                groups(i).ImgType = groups(i).subject(j).imgType;
                log = sprintf('%s\n%s %s %s', log, datestr(now), 'Checking image type - img: ',  groups(i).subject(j).imgType);

            elseif(groups(i).ImgType ~= groups(i).subject(j).imgType)
                log = sprintf('%s\n%s %s %s', log, datestr(now), 'Different Image Types in group/subject folders: ',  groups(i).subject(j).imgType);
                return; % This group has multiple image types
            end


        end
    end
    %
    % Start DICOM Processing

    %Test dcm Time Points

    if (ImgType == 1) 

       log = sprintf('%s\n%s %s', log, datestr(now), 'dcm time points processing: ');
       for i = 1:numGroups
    %     
        for j = 1:groups(i).numSubjects  
            log = sprintf('%s\n%s %s %s', log, datestr(now), 'dcm time points processing: ', groups(i).subject(j).name);
               if (j == 1)
                    numFiles = groups(i).subject(j).dcm; 
                    groups(i).timepoints = groups(i).subject(j).dcm;

               elseif (numFiles == groups(i).subject(j-1).dcm)
                    groups(i).timepoints = groups(i).subject(j).dcm;
                    log = sprintf('%s\n%s %s %s', log, datestr(now), 'dcm time points processing: ', groups(i).subject(j-1).name);

               else
                    flag_FILE_TYPE = 1;
                    groups(i).timepoints = 0;
                    log = sprintf('%s\n%s %s %s %s', log, datestr(now), 'Non-uniform number of dcm time points likely: ', groups(i).name, groups(i).subject(j-1).name);
                    return;
               end



        end
       end

    % Start Convert to Img/Hdr

       for i = 1:numGroups

          log = sprintf('%s\n%s %s', log, datestr(now), 'Creating Analysis Folder: ');
          mkdir(groups(i).path, 'Analysis');
          groups(i).path = fullfile(groups(i).path, 'Analysis');
          log = sprintf('%s\n%s %s %s', log, datestr(now), 'Created Analysis Folder: ', groups(i).path);
          log = sprintf('%s\n%s %s', log, datestr(now), 'Creating FunImg Folder: ');
          mkdir(groups(i).path, 'FunImg');
          groups(i).funimgpath = fullfile(groups(i).path, 'FunImg');
          log = sprintf('%s\n%s %s %s', log, datestr(now), 'Created FunImg Folder: ', groups(i).funimgpath);

        for j = 1:groups(i).numSubjects

            subFolder = groups(i).subject(j).name;
            log = sprintf('%s\n%s %s', log, datestr(now), 'Creating SubFolder: ');
            mkdir(groups(i).funimgpath, subFolder);
            groups(i).subject(j).funimgpath = fullfile(groups(i).funimgpath, subFolder);
            log = sprintf('%s\n%s %s %s', log, datestr(now), 'Created SubFolder: ', groups(i).subject(j).funimgpath);
            source = groups(i).subject(j).path;
            log = sprintf('%s\n%s %s %s', log, datestr(now), 'source: ', source);
            dest = groups(i).subject(j).funimgpath;
            log = sprintf('%s\n%s %s %s', log, datestr(now), 'dest: ', dest);
            option='-b ./dcm2nii_linux.ini';
            log = sprintf('%s\n%s %s %s', log, datestr(now), 'option: ', option);
            cd (fullfile(DPARSF_PATH, 'dcm2nii'))
            eval('!chmod +x dcm2nii_linux');
            eval(['!./dcm2nii_linux ',option,' -o ',dest,' ',source]);

            cd (SOFT_PATH);

            hdr = dir([groups(i).subject(j).funimgpath, '/*.hdr']);
            groups(i).subject(j).hdr = length(hdr(not([hdr.isdir])));
            log = sprintf('%s\n%s %s %s', log, datestr(now), 'group subject header: ', groups(i).subject(j).hdr);

            img = dir([groups(i).subject(j).funimgpath, '/*.img']);
            groups(i).subject(j).img = length(img(not([img.isdir])));
            log = sprintf('%s\n%s %s %s', log, datestr(now), 'group subject image: ', groups(i).subject(j).img);

        end
       end

    % End Convert to Img/Hdr

    %Check the number of time points after conversion to Img/Hdr
    for i = 1:numGroups

        for j = 1:groups(i).numSubjects
        % Dcm images not fully converted
        log = sprintf('%s\n%s %s', log, datestr(now), 'Comparing the number of converted img/hdr from dcm: ');
            if(groups(i).timepoints ~= groups(i).subject(j).img) 
                groups(i).name;
                groups(i).subject(j).name;
                groups(i).timepoints;
                groups(i).subject(j).img;
                log = sprintf('%s\n%s %s %s', log, datestr(now), 'group name: ', groups(i).name);
                log = sprintf('%s\n%s %s %s', log, datestr(now), 'group subject name: ', groups(i).subject(j).name);
                log = sprintf('%s\n%s %s %s', log, datestr(now), 'group time points: ', groups(i).timepoints);
                log = sprintf('%s\n%s %s %s', log, datestr(now), 'group subject image: ', groups(i).subject(j).img);
                %return;
            end

        end

    end

    end



    %End DICOM Processing


    % Starting with Img/Hdr
    % Check the number of Img/ Hdr

    if (ImgType == 0)

     log = sprintf('%s\n%s %s', log, datestr(now), 'Started with Img/Hdr: ');
     for i = 1:numGroups

        for j = 1:groups(i).numSubjects

                hdr = dir([groups(i).subject(j).path, '/*.hdr']);
                groups(i).subject(j).hdr = length(hdr(not([hdr.isdir])));

                if (groups(i).subject(j).hdr ~= groups(i).subject(j).img)
                    log = sprintf('%s\n%s %s %s %s', log, datestr(now), 'Non-uniform Img/Hdr: ',groups(i).subject(j).hdr, groups(i).subject(j).img);
                    log = sprintf('%s\n%s %s %s %s', log, datestr(now), 'Non-uniform Img/Hdr: ',groups(i).name, groups(i).subject(j).name);
                    flag_FILE_TYPE = 1;
                    return;
                else

                    imageType = 0;
                    dirType = 'FunImg';

                    if (j == 1)
                        numFiles = groups(i).subject(j).img; 
                        groups(i).timepoints = groups(i).subject(j).img;
                        log = sprintf('%s\n%s %s %s %s', log, datestr(now), 'Setting group time points: ', groups(i).name, groups(i).timepoints);

                    elseif (numFiles == groups(i).subject(j-1).img) % Compare time points of subject j to subject j - 1
                        groups(i).timepoints = groups(i).subject(j).img;
                        log = sprintf('%s\n%s %s %s %s', log, datestr(now), 'Setting group time points: ', groups(i).name, groups(i).timepoints);    
                    else
                        log = sprintf('%s\n%s %s %s %s', log, datestr(now), 'Group/ Subject Timepoint setting problem: ', numFiles, groups(i).subject(j-1).img); 
                        flag_FILE_TYPE = 1;
                        groups(i).timepoints = 0;
                        return;
                    end
                end

        end
     end
    end

    % End Img/Hdr


    % 
    % Create an analysis folder for each group
    % ../Group/Analysis/FunRaw or FunImg
    

    if (ImgType == 0) 

        for i=1:numGroups

          
           mkdir(folder_name, 'temp');
           temploc = fullfile (folder_name, 'temp');
           log = sprintf('%s\n%s %s %s', log, datestr(now), 'Creating temp location: ', temploc); 


           cmd_str = sprintf('cp -a %s%s %s%s',groups(i).path, '/.', temploc, '/');
           [status, result] = system(cmd_str)
           log = sprintf('%s\n%s %s %s', log, datestr(now), 'temp location result: ', result);      
    %        cmd_str = sprintf('rd /S /Q %s', groups(i).path);
    %        [status, result] = system(cmd_str)
    %        mkdir(groups(i).path);
    %        
           mkdir(groups(i).path, 'Analysis');
           groups(i).path = [groups(i).path '/Analysis'];
           log = sprintf('%s\n%s %s %s', log, datestr(now), 'Creating Analysis Folder: ', groups(i).path); 
           mkdir(groups(i).path, dirType);
           groups(i).WorkingDir = fullfile(groups(i).path, dirType);
           log = sprintf('%s\n%s %s %s', log, datestr(now), 'Working Directory set: ', groups(i).WorkingDir);  
           cmd_str = sprintf('cp -a %s%s %s%s',temploc, '/.', groups(i).WorkingDir, '/'); 
           [status, result] = system(cmd_str)
           log = sprintf('%s\n%s %s %s %s', log, datestr(now), 'Copy to temp location: ', status, result);  

           cmd_str = sprintf('rm -rf %s', temploc);
           [status, result] = system(cmd_str)
           log = sprintf('%s\n%s %s %s %s', log, datestr(now), 'Removing temp location: ', status, result);  
          % groups(i).analysis_path = fullfile(groups(i).path, 'Analysis')

        end
    end
    
    log = sprintf('%s\n%s %s', log, datestr(now), 'PrepEnvironment Completed ');  
    log = strrep(log, '\', '\\');
    fileID = fopen(log_fn, open);
    fprintf(fileID, log);
    fclose(fileID);

catch e
    
    error = 1;
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'Main Error, Identifier: ', e.identifier)
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'Main Error, Message: ', e.message)
    log = strrep(log, '\', '\\');
    fileID = fopen(log_fn, open);
    fprintf(fileID, log);
    fclose(fileID);
    return;
end
