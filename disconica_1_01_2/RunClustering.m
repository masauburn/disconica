function max = RunClustering(groups, pathstr)

formatOut = 'mm_dd_yy';
log_fn = ['disconica_log_' datestr(now,formatOut) '.log'];
% soft = mfilename('fullpath');
% [pathstr,name,ext] = fileparts(soft);
% 
% if (~isempty(parameters.path_name))
%     pathstr = parameters.path_name
% end

addpath(pathstr);

log_loc = fullfile(pathstr, 'logs');

if exist(log_loc, 'dir') ~= 7
    mkdir(log_loc);
end

log_fn = fullfile(log_loc, log_fn);

if exist(log_fn, 'file') == 2
    open = 'w';
else
    open = 'a';
end

%
log = sprintf('%s %s', datestr(now), 'Run Clustering Started');
log = sprintf('%s\n%s %s', log, datestr(now), 'Setting up initialization parameters');

fileID = fopen(log_fn, open);
fprintf(fileID, log);
fclose(fileID);
error = 0;
max = [];
numGroups = length(groups);

for i = 1:numGroups
            
    subjects(i) = numel(groups(i).melodicFiles);
            
end

try
    i = 1;
    r = 1;
    flag = 0;
    result = [];
    foundComp = 0;
    

    for j = 1:numel(groups(i).inds)

                i = 1;
                a(j).vec = groups(i).x_vec(j).vec;
                a(j).mean = mean(a(j).vec);
                next = i+1

                log = sprintf('%s\n%s %s', log, datestr(now), 'Combining and running KMeans:');
                if (next <= numGroups && foundComp == 0)

                    for k = 1:numel(groups(next).inds)
                        if (foundComp == 0)

                            b(k).vec = [a(j).vec; groups(next).x_vec(k).vec];
                            b(k).mean = [a(j).mean; mean(groups(next).x_vec(k).vec)];

                            next = next + 1;

                            if (next <= numGroups && flag == 0 && foundComp == 0)
                                % 3 Groups
                                for l = 1:numel(groups(next).inds)

                                    z(l).vec = [b(k).vec; groups(next).x_vec(l).vec];
                                    z(l).mean = [b(k).mean; mean(groups(next).x_vec(l).vec)];

                                    result(r).numGroups = numGroups;
                                    result(r).subjects = subjects;
                                    result(r).vec = z(l).vec;
                                    result(r).mean = z(l).mean;
                                    % Call Clustering
                                    result(r).g = KMeans(result(r).numGroups, result(r).subjects, result(r).vec, result(r).mean);
                                    p1 = result(r).g(i).purity
                                    p2 = result(r).g(i+1).purity
                                    p3 = result(r).g(i+2).purity
                                    cp = p1 + p2 + p3
                                    
                                    if (l == 1)

                                        max.maxval = cp
                                        max.groups(i).name = groups(i).name;
                                        max.groups(i).currComp = groups(i).x_vec(j).ind;
                                        max.groups(i).currcompvec = groups(i).x_vec(j).vec;
                                        max.groups(i+1).name = groups(i+1).name;
                                        max.groups(i+1).currComp = groups(i+1).x_vec(k).ind;
                                        max.groups(i+1).currcompvec = groups(i+1).x_vec(k).vec;
                                        max.groups(i+2).name = groups(i+2).name;
                                        max.groups(i+2).currComp = groups(i+2).x_vec(l).ind;
                                        max.groups(i+2).currcompvec = groups(i+2).x_vec(l).vec;

                                    elseif (cp > max.maxval)

                                        max.maxval = cp
                                        max.groups(i).name = groups(i).name;
                                        max.groups(i).currComp = groups(i).x_vec(j).ind;
                                        max.groups(i).currcompvec = groups(i).x_vec(j).vec;
                                        max.groups(i+1).name = groups(i+1).name;
                                        max.groups(i+1).currComp = groups(i+1).x_vec(k).ind;
                                        max.groups(i+1).currcompvec = groups(i+1).x_vec(k).vec;
                                        max.groups(i+2).name = groups(i+2).name;
                                        max.groups(i+2).currComp = groups(i+2).x_vec(l).ind;
                                        max.groups(i+2).currcompvec = groups(i+2).x_vec(l).vec;

                                    end
                                    
                                    
                                    %End Call Clustering
                                    result(r).groups(i).name = groups(i).name;
                                    result(r).groups(i).currComp = groups(i).x_vec(j).ind;
                                    result(r).groups(i+1).name = groups(i+1).name;
                                    result(r).groups(i+1).currComp = groups(i+1).x_vec(k).ind;
                                    result(r).groups(i+2).name = groups(i+2).name;
                                    result(r).groups(i+2).currComp = groups(i+2).x_vec(l).ind;

                                    r = r + 1;
                                    next = next - 1;

                                end

                            else
                                % 2 Groups

                                flag = 1;

                                %confMatrix = KMeans(numGroups, subjects, b(k).vec, b(k).mean);
                                %result(r).confMatrix = confMatrix;
                                result(r).numGroups = numGroups;
                                result(r).subjects = subjects;
                                result(r).vec = b(k).vec;
                                result(r).mean = b(k).mean;
                                % Call Clustering
                                result(r).g = KMeans(result(r).numGroups, result(r).subjects, result(r).vec, result(r).mean);
                                p1 = result(r).g(i).purity
                                p2 = result(r).g(i+1).purity

                                if (k == 1)

                                    max.maxval = p1 + p2
                                    max.groups(i).name = groups(i).name;
                                    max.groups(i).currComp = groups(i).x_vec(j).ind;
                                    max.groups(i).currcompvec = groups(i).x_vec(j).vec;
                                    max.groups(i+1).name = groups(i+1).name;
                                    max.groups(i+1).currComp = groups(i+1).x_vec(k).ind;
                                    max.groups(i+1).currcompvec = groups(i+1).x_vec(k).vec;

                                elseif (p1 + p2 > max.maxval)

                                    max.maxval = p1 + p2
                                    max.groups(i).name = groups(i).name;
                                    max.groups(i).currComp = groups(i).x_vec(j).ind;
                                    max.groups(i).currcompvec = groups(i).x_vec(j).vec;
                                    max.groups(i+1).name = groups(i+1).name;
                                    max.groups(i+1).currComp = groups(i+1).x_vec(k).ind;
                                    max.groups(i+1).currcompvec = groups(i+1).x_vec(k).vec;

                                end
    %                             p1 + p2
    %                             if (p1 + p2 > 1.9)
    %                                 max.groups(i).name = groups(i).name;
    %                                 max.groups(i).currComp = groups(i).x_vec(j).ind;
    %                                 max.groups(i).currcompvec = groups(i).x_vec(j).vec;
    %                                 max.groups(i+1).name = groups(i+1).name;
    %                                 max.groups(i+1).currComp = groups(i+1).x_vec(k).ind;
    %                                 max.groups(i+1).currcompvec = groups(i+1).x_vec(k).vec;
    %                                 foundComp = 1;
    %                             end
                                %End Call Clustering
                                result(r).groups(i).name = groups(i).name;
                                result(r).groups(i).currComp = groups(i).x_vec(j).ind;
                                result(r).groups(i+1).name = groups(i+1).name;
                                result(r).groups(i+1).currComp = groups(i+1).x_vec(k).ind;
                                r = r + 1;
                                next = next - 1;
                            end

                        end
                    end


                end




    end
      
    log = sprintf('%s\n%s %s', log, datestr(now), 'Ending Run Clustering');
    fileID = fopen(log_fn, open);
    fprintf(fileID, log);
    fclose(fileID);
    
catch e
    
    error = 1;
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'Main Error, Identifier: ', e.identifier)
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'Main Error, Message: ', e.message)
    log = strrep(log, '\', '\\');
    log = sprintf('%s\n%s %s', log, datestr(now), 'Run clustering did not complete successfully');
    fileID = fopen(log_fn, open);
    fprintf(fileID, log);
    fclose(fileID);
    
end
    
