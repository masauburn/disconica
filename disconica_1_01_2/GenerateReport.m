import mlreportgen.report.*
 
report = Report('MyJet');

titlepg = TitlePage;
titlepg.Title = 'My Jet';
titlepg.Author = 'Myself';
add(report,titlepg);

add(report,TableOfContents);

chap = Chapter('My Jet');
add(chap,'Here is my jet:');
add(chap,FormalImage('Image',...
    which('b747.jpg'),'Height','5in',...
    'Width','5in','Caption','Boeing 747'));
add(report,chap);
close(report);
rptview(report);