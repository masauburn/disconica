function [error, groups] = PrepRunDPARSF(parameters, groups)

error = 0;


soft = mfilename('fullpath');
[pathstr,name,ext] = fileparts(soft);

if (~isempty(parameters.path_name))
    pathstr = parameters.path_name
end
addpath(pathstr)
formatOut = 'mm_dd_yy';
log_fn = ['disconica_log_' datestr(now,formatOut) '.log'];
log_loc = fullfile(pathstr, 'logs');

if exist(log_loc, 'dir') ~= 7
    mkdir(log_loc);
end

log_fn = fullfile(log_loc, log_fn);

if exist(log_fn, 'file') == 2
    open = 'a';
else
    open = 'w';
end


log = sprintf('%s %s', datestr(now), 'PrepRunDPARSF Started')
log = sprintf('%s\n%s %s', log, datestr(now), 'Setting up initialization parameters')

fileID = fopen(log_fn, open);
fprintf(fileID, log);
fclose(fileID);

try
    
    NIFTI_PATH = sprintf('%s/%s', pathstr, 'NIfTI');
    SPM_PATH = sprintf('%s/%s', pathstr, 'spm8');
    REST_PATH = sprintf('%s/%s', pathstr, 'REST18');
    DPARSF_PATH = sprintf('%s/%s/%s', pathstr, 'DPABI','DPARSF');
    GR_PATH = sprintf('%s/%s', pathstr, 'gRAICAR');
    TEMP_PATH = sprintf('%s/%s', pathstr, 'template.fsf');
    STD_BRAIN_PATH = sprintf('%s/%s', pathstr, 'standard/MNI152_T1_2mm_brain');
    DCM_PATH = sprintf('%s/%s', DPARSF_PATH, 'dcm2nii');
    DPABI_PATH = sprintf('%s/%s', pathstr, 'DPABI');
    SOFT_PATH = pathstr;
    
     % add path to gRAICAR
    addpath(GR_PATH);
    % add path to NIFTI Software
    addpath(NIFTI_PATH);
    % % add path to SPM
    addpath(SPM_PATH);
    % % add path to REST
    addpath(REST_PATH);
    % % add path to DPARSF
    addpath(DPARSF_PATH);
    %addpath('C:\Users\Saidah\Desktop\PTSDStuff\DPABI\DPABI_V1.3_150710\DPARSF_V3.2_150710'); 
    addpath(DPABI_PATH); 
    addpath(fullfile(DPABI_PATH, 'Subfunctions'));
    addpath(fullfile(DPARSF_PATH, 'Subfunctions'));
    addpath(fullfile(DPARSF_PATH, 'Jobmats'));
    % Starting DPARSF

    log = sprintf('%s\n%s %s', log, datestr(now), 'Collecting Input parameters ');
    %parameters = collect_Inputs(handles); % These settings are constant for all groups

    % DPARSF Settings
    numGroups = numel(groups);
    
    
    for i = 1:numGroups % i corresponds to group index

        log = sprintf('%s\n%s %s', log, datestr(now), 'Setting Input parameters ');
        groups(i).cfg.SubjectID = {};
        groups(i).cfg.WorkingDir = groups(i).path; 
        groups(i).cfg.DataProcessDir =groups(i).path;
        groups(i).cfg.SubjectID={};
        
        for j = 1:groups(i).numSubjects % subjects in group i

            groups(i).cfg.SubjectID{j,1} = groups(i).subject(j).name;

        end

        groups(i).cfg.TimePoints=groups(i).timepoints;
        groups(i).cfg.TR= parameters.TR;
        groups(i).cfg.IsNeedConvertFunDCM2IMG=0;
        groups(i).cfg.IsRemoveFirstTimePoints= parameters.IsRemoveFirstTimePoints;  %1
        groups(i).cfg.RemoveFirstTimePoints= parameters.RemoveFirstTimePoints; %10
        groups(i).cfg.IsSliceTiming= parameters.IsSliceTiming; %1;
        groups(i).cfg.SliceTiming.SliceNumber= str2num(parameters.SliceNumber);
        groups(i).cfg.SliceTiming.TR= parameters.TR;%groups(i).cfg.TR;
        groups(i).cfg.SliceTiming.TA=groups(i).cfg.SliceTiming.TR-(groups(i).cfg.SliceTiming.TR/groups(i).cfg.SliceTiming.SliceNumber);
        groups(i).cfg.SliceTiming.SliceOrder= [1:2:33,2:2:32];
        groups(i).cfg.SliceTiming.ReferenceSlice= str2num(parameters.ReferenceSlice); %33;
        groups(i).cfg.IsRealign= parameters.IsRealign; %1;
        groups(i).cfg.IsNormalize= parameters.IsNormalize; 
        groups(i).cfg.IsNeedConvertT1DCM2IMG=0;
        groups(i).cfg.Normalize.BoundingBox = eval(['[',parameters.BoundingBox,']']); %[-90 -126 -72;90 90 108];
        groups(i).cfg.Normalize.VoxSize= eval(['[',parameters.VoxSize,']']); %[2 2 2];
        groups(i).cfg.Normalize.AffineRegularisationInSegmentation='mni';
        groups(i).cfg.IsDelFilesBeforeNormalize=1;
        groups(i).cfg.IsSmooth= parameters.IsSmooth; %1;
        groups(i).cfg.Smooth.FWHM= eval(['[',parameters.FWHM,']']);  %[4 4 4];
        if (groups(i).cfg.IsSmooth == 1)
            groups(i).cfg.DataIsSmoothed=1;
        else
            groups(i).cfg.DataIsSmoothed=0;
        end
        %groups(i).cfg.DataIsSmoothed=1; 
        groups(i).cfg.IsDetrend= parameters.IsDetrend; %1; 
        groups(i).cfg.IsFilter= parameters.IsFilter; %1;
        groups(i).cfg.Filter.ASamplePeriod= parameters.TR;
        groups(i).cfg.Filter.AHighPass_LowCutoff= str2double(parameters.AHighPass_LowCutoff); %0.01;
        groups(i).cfg.Filter.ALowPass_HighCutoff= str2double(parameters.ALowPass_HighCutoff); %0.1;
        groups(i).cfg.Filter.AMaskFilename='';
        groups(i).cfg.Filter.AAddMeanBack='Yes';  
        groups(i).cfg.IsDelDetrendedFiles=0;

        groups(i).cfg.MaskFile ='Default';
        groups(i).cfg.IsCalReHo=0;
        groups(i).cfg.CalReHo.ClusterNVoxel=27;
        groups(i).cfg.CalReHo.AMaskFilename='Default';
        groups(i).cfg.CalReHo.smReHo=0;
        groups(i).cfg.CalReHo.mReHo_1=0;
        groups(i).cfg.IsCalALFF=0;
        groups(i).cfg.CalALFF.ASamplePeriod=0;
        groups(i).cfg.CalALFF.AHighPass_LowCutoff=0;
        groups(i).cfg.CalALFF.ALowPass_HighCutoff=0;
        groups(i).cfg.CalALFF.AMaskFilename='Default';
        groups(i).cfg.CalALFF.mALFF_1=0;
        groups(i).cfg.IsCalfALFF=0;
        groups(i).cfg.CalfALFF.ASamplePeriod=0;
        groups(i).cfg.CalfALFF.AHighPass_LowCutoff=0;
        groups(i).cfg.CalfALFF.ALowPass_HighCutoff=0;
        groups(i).cfg.CalfALFF.AMaskFilename='Default';
        groups(i).cfg.CalfALFF.mfALFF_1=0;
        groups(i).cfg.IsCovremove=0;
        groups(i).cfg.Covremove.HeadMotion=0;
        groups(i).cfg.Covremove.WholeBrain=0;
        groups(i).cfg.Covremove.CSF=0;
        groups(i).cfg.Covremove.WhiteMatter=0;
        groups(i).cfg.Covremove.OtherCovariatesROI=[]; %YAN Chao-Gan added 091215./091212.
        groups(i).cfg.IsExtractAALTC=0;
        groups(i).cfg.IsExtractROITC=0;
        groups(i).cfg.ExtractROITC.IsTalCoordinates=0;
        groups(i).cfg.ExtractROITC.ROICenter='';%ROICenter;
        groups(i).cfg.ExtractROITC.ROIRadius=0;

        groups(i).cfg.IsExtractRESTdefinedROITC=0;
        groups(i).cfg.IsCalFC=0;
        groups(i).cfg.CalFC.ROIDef=[];
        groups(i).cfg.CalFC.AMaskFilename='Default';

        groups(i).cfg.IsResliceT1To1x1x1=0;
        groups(i).cfg.IsT1Segment=0;
        groups(i).cfg.IsWrapAALToNative=0;
        groups(i).cfg.IsExtractAALGMVolume=0; 
        groups(i).cfg.DPARSFVersion = 'V4.3_170105';
        groups(i).cfg.Covremove.PolynomialTrend = 0; %YAN Chao-Gan. 140815
        groups(i).cfg.IsExtractROISignals = 0;
        groups(i).cfg.CalFC.IsMultipleLabel = 0;

        groups(i).cfg.StartingDirName = 'FunImg';

    end

    % Start Log DPARSF Parameters
    for i = 1:numGroups 

        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Group name: ', groups(i).name);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Working dir: ', groups(i).cfg.WorkingDir); 
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Data process dir: ', groups(i).cfg.DataProcessDir); 

        for j = 1:groups(i).numSubjects % subjects in group i
            log = sprintf('%s\n%s %s %s', log, datestr(now), 'Subj: ', groups(i).cfg.SubjectID{j,1});
        end

        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Time points: ', groups(i).cfg.TimePoints);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'TR: ', groups(i).cfg.TR);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'ConvFunDCM2IMG: ', groups(i).cfg.IsNeedConvertFunDCM2IMG);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Remove First Time points: ', groups(i).cfg.IsRemoveFirstTimePoints);  %1
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Time points: ', groups(i).cfg.RemoveFirstTimePoints); %10
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Slice Timing: ', groups(i).cfg.IsSliceTiming); %1;
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Slice Num: ', groups(i).cfg.SliceTiming.SliceNumber);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Slice Timing TR: ', groups(i).cfg.SliceTiming.TR);%groups(i).cfg.TR;
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Slice Timing TA: ', groups(i).cfg.SliceTiming.TA);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Slice Order: ', groups(i).cfg.SliceTiming.SliceOrder);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Reference Slices: ', groups(i).cfg.SliceTiming.ReferenceSlice); %33;
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Is Realign: ', groups(i).cfg.IsRealign); %1;
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Is Normalize: ', groups(i).cfg.IsNormalize); 
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Conv. DCM2IMG: ',groups(i).cfg.IsNeedConvertT1DCM2IMG);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Bounding Box: ', groups(i).cfg.Normalize.BoundingBox); %[-90 -126 -72;90 90 108];
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Vox Size: ', groups(i).cfg.Normalize.VoxSize); %[2 2 2];
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Affine Regularization: ', groups(i).cfg.Normalize.AffineRegularisationInSegmentation);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Del. before norm.: ', groups(i).cfg.IsDelFilesBeforeNormalize);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Is Smooth: ', groups(i).cfg.IsSmooth); %1;
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'FWHM: ', groups(i).cfg.Smooth.FWHM);  %[4 4 4];
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Data Smoothed: ', groups(i).cfg.DataIsSmoothed);
        %groups(i).cfg.DataIsSmoothed=1; 
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Is Detrend: ', groups(i).cfg.IsDetrend); %1; 
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Is Filter: ', groups(i).cfg.IsFilter); %1;
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Asample Per.: ', groups(i).cfg.Filter.ASamplePeriod);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'AHigh pass low cutoff: ', groups(i).cfg.Filter.AHighPass_LowCutoff); %0.01;
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'ALow Pass high cutoff: ',groups(i).cfg.Filter.ALowPass_HighCutoff); %0.1;
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'AMask: ',groups(i).cfg.Filter.AMaskFilename);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Add Mean Back: ', groups(i).cfg.Filter.AAddMeanBack);  
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Delete detr. files: ',groups(i).cfg.IsDelDetrendedFiles);

        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Mask File: ', groups(i).cfg.MaskFile );
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Iscalreho: ',groups(i).cfg.IsCalReHo);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Clust. Vox: ',groups(i).cfg.CalReHo.ClusterNVoxel);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Maskfile: ',groups(i).cfg.CalReHo.AMaskFilename);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'smReHo: ', groups(i).cfg.CalReHo.smReHo);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'mReHo_l: ',groups(i).cfg.CalReHo.mReHo_1);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Iscalalff: ', groups(i).cfg.IsCalALFF);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'calalff sample period: ',groups(i).cfg.CalALFF.ASamplePeriod);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'calalff ahighpass low cutoff: ',groups(i).cfg.CalALFF.AHighPass_LowCutoff);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'calalff alow pass high cutoff: ', groups(i).cfg.CalALFF.ALowPass_HighCutoff);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Mask File: ', groups(i).cfg.CalALFF.AMaskFilename);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'malff: ', groups(i).cfg.CalALFF.mALFF_1);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'iscalfalff: ', groups(i).cfg.IsCalfALFF);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Asample period: ',groups(i).cfg.CalfALFF.ASamplePeriod);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Ahigh pass low cutoff: ',groups(i).cfg.CalfALFF.AHighPass_LowCutoff);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Alow pass high cutoff: ',groups(i).cfg.CalfALFF.ALowPass_HighCutoff);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Mask File: ',groups(i).cfg.CalfALFF.AMaskFilename);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'mfalff_l: ', groups(i).cfg.CalfALFF.mfALFF_1);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Iscovremove: ', groups(i).cfg.IsCovremove);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'cov rem head motion: ',groups(i).cfg.Covremove.HeadMotion);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'cov rem whole brain: ',groups(i).cfg.Covremove.WholeBrain);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'cov rem csf: ', groups(i).cfg.Covremove.CSF);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'cov rem white matter: ',groups(i).cfg.Covremove.WhiteMatter);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'cov remove other covariates: ',groups(i).cfg.Covremove.OtherCovariatesROI); 
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'is ext aaltc: ', groups(i).cfg.IsExtractAALTC);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'is ext roitc: ',groups(i).cfg.IsExtractROITC);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'is tal coordinates: ', groups(i).cfg.ExtractROITC.IsTalCoordinates);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'roi center: ', groups(i).cfg.ExtractROITC.ROICenter);%ROICenter;
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'roi radius: ', groups(i).cfg.ExtractROITC.ROIRadius);

        log = sprintf('%s\n%s %s %s', log, datestr(now), 'rest def roitc: ',groups(i).cfg.IsExtractRESTdefinedROITC);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'is cal fc: ',groups(i).cfg.IsCalFC);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'roi def: ', groups(i).cfg.CalFC.ROIDef);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'CalFC Amask file name: ', groups(i).cfg.CalFC.AMaskFilename);

        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Reslice t1to1x1x1: ', groups(i).cfg.IsResliceT1To1x1x1);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'ist1segment: ', groups(i).cfg.IsT1Segment);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Wrap AAL to native: ', groups(i).cfg.IsWrapAALToNative);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'AALGMvol: ', groups(i).cfg.IsExtractAALGMVolume); 
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'DPARSF ver: ',groups(i).cfg.DPARSFVersion);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'rem polynom. trend: ', groups(i).cfg.Covremove.PolynomialTrend); 
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Is ext roi signals: ',groups(i).cfg.IsExtractROISignals);
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Is Multiple Label CalFC: ', groups(i).cfg.CalFC.IsMultipleLabel);

        log = sprintf('%s\n%s %s %s', log, datestr(now), 'Start dir name: ', groups(i).cfg.StartingDirName);
    end
    % End Log DPARSF Parameters

    % %Test DPARSF
    % 
    % 
    % %trying DPARSF Direct
    tic

    % spm fmri
    % DPARSF Processing
    %parfor i = 1:numGroups
    
    log = sprintf('%s\n%s %s', log, datestr(now), 'Starting DPARSF Analysis: ');

    for index = 1:numGroups

        [Error]=DPARSF_run(groups(index).cfg)

    end

    log = sprintf('%s\n%s %s %s', log, datestr(now), 'Elapsed Time for DPARSF: ', toc);
    log = sprintf('%s\n%s %s', log, datestr(now), 'DPARSF Completed: ');
    log = strrep(log, '\', '\\');
    fileID = fopen(log_fn, open);
    fprintf(fileID, log);
    fclose(fileID);

catch
    
    error = 1;
    log = sprintf('%s\n%s %s', log, datestr(now), 'PrepRunDPARSF did not complete successfully: ');
    log = strrep(log, '\', '\\');
    fileID = fopen(log_fn, open);
    fprintf(fileID, log);
    fclose(fileID);
    
end