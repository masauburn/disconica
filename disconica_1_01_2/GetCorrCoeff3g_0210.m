function [index path_2 R_P R_Det] = GetCorrCoeff3g(gcc, ax, ay, az)

%path_1 = '/home/mri_s2/mas0047/ABIDE_DATA/gR_Data/Autism/gRSecondRun/compMaps/comp4.nii';
path_1 = gcc.maxComp;
%path = '/home/mri_s2/mas0047/gR_Data/Autism/0050002/filtered_func_data.ica/melodic_IC.nii'
a_1 = load_nii(path_1)
x_1 = squeeze(a_1.img(:,:,:));
x_1 = reshape (x_1,1, ax*ay*az);
v_size = numel(gcc.compMaps)
%Control_Components = {2,5,7};
    for v = 1:2

        dirName = gcc.compMaps(v).val;
        dirData = dir(dirName);      
        %# Get the data for the current directory
        dirIndex = [dirData.isdir];  %# Find the index for directories
        fileList = {dirData(~dirIndex).name}';  %'# Get a list of the files
        index(v) = 0
            for i = 1:length(fileList)
                path_2 = [dirName '/' fileList{i}];
                a_2 = load_nii(path_2)
                x_2 = squeeze(a_2.img(:,:,:));
                x_2 = reshape (x_2,1,ax*ay*az);
                [R, P] = corrcoef(x_1, x_2);
                if (i == 1)
                    R_P = det(P);
                    R_Mat = R;
                    R_Det = det(R);
                    index(v) = i
                elseif det(R) > R_Det
                        R_Det = det(R);
                        R_P = det(P); % current p-value
                        index(v) = i

                end
               %i = i+1; 

            end
        
    end

end

