function [index path_2 R_P R_Det] = GetCorrCoeff3g(gcc, ax, ay, az)

%path_1 = '/home/mri_s2/mas0047/ABIDE_DATA/gR_Data/Autism/gRSecondRun/compMaps/comp4.nii';
path_1 = gcc.maxComp;
%path = '/home/mri_s2/mas0047/gR_Data/Autism/0050002/filtered_func_data.ica/melodic_IC.nii'
a_1 = load_nii(path_1);
x_1 = squeeze(a_1.img(:,:,:));
x_1 = reshape (x_1,1, ax*ay*az);
v_size = numel(gcc.compMaps);
%path_2(1).value = 'None';
%Control_Components = {2,5,7};
    for v = 1:2

        dirName = gcc.compMaps(v).val;
        dirData = dir(dirName);      
        %# Get the data for the current directory
        dirIndex = [dirData.isdir];  %# Find the index for directories
        fileList = {dirData(~dirIndex).name}';  %'# Get a list of the files
        index(v).value = 0;
            for i = 1:length(fileList)
                path_2(v).value = [dirName '/' fileList{i}];
                a_2 = load_nii(path_2(v).value);
                x_2 = squeeze(a_2.img(:,:,:));
                x_2 = reshape (x_2,1,ax*ay*az);
                [R, P] = corrcoef(x_1, x_2);
                if (i == 1)
                    
                    
                    R_Det(v).value = det(R);
                    R_P(v).value = det(P);
                    index(v).value = i
                
                elseif det(R) > R_Det(v).value
                        
                    path_2(v).value = [dirName '/' fileList{i}];
                    R_Det(v).value = det(R);
                    R_P(v).value = det(P); % current p-value
                    index(v).value = i

                end
               %i = i+1; 

            end
        
    end

end

