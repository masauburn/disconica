path_1 = '/home/mas0047/PTSD_ANALYSIS/PTSD/Melodic/gR/grout/compMaps/comp052.nii.gz';
%path = '/home/mri_s2/mas0047/gR_Data/Autism/0050002/filtered_func_data.ica/melodic_IC.nii'
a_1 = load_nii(path_1)
size(a_1.img)
x_1 = squeeze(a_1.img(:,:,:));
x_1 = reshape (x_1,1, 64*64*16);
parpool(4);

%Control_Components = {2,5,7};
dirName = '/home/mas0047/PTSD_ANALYSIS/PCSP/Melodic/gR/grout/compMaps';
dirData = dir(dirName);      
%# Get the data for the current directory
dirIndex = [dirData.isdir];  %# Find the index for directories
fileList = {dirData(~dirIndex).name}';  %'# Get a list of the files

imgList = cell(1,length(fileList));

parfor i = 1:length(fileList)
    
    fname = fileList{i}
    path_2 = strcat(dirName, '/' , fileList{i})
    a_2 = load_nii(path_2)
    size(a_2.img)
    x_2 = squeeze(a_2.img(:,:,:));
    x_2 = reshape (x_2,1,64*64*16);
    imgList{i} = x_2;
    
end

R_Det = cell(1,length(fileList));

 parfor i = 1:length(imgList)  
    [R, P] = corrcoef(x_1, imgList(i))
    
        R_Mat = R;
        R_Det{i} = det(R);
        
    

 end
 
  length(R_Det);
  [M,I] = max(R_Det);
  M
  I
%i


% for i = 1:3
% 
% path_2 = strcat('/home/mri_s2/mas0047/gR_Data/Control/gRSecondRun_C/compMaps/comp', num2str(Control_Components{i}), '.nii')
% 
% 
% a_2 = load_nii(path_2)
% size(a_2.img)
% x_2 = squeeze(a_2.img(:,:,:));
% x_2 = reshape (x_2,1,61*73*61);
% [R, P] = corrcoef(x_1, x_2)
% 
% end

%x_vec = reshape(a, 1, 1)
%x_vec
% x = squeeze(a.img(:,:,:,ICIndex(i+1)));
% 
% 
% 
% 
% 
% 
% load ('/home/mri_s2/mas0047/gR_Data/Autism/gRSecondRun/task_result.mat'); %# Load task_result.mat
% addpath('/home/mri_s2/mas0047/NIFTI_ANALYZE');
% 
% Control_Components = {5,2,7};
% Autism_Components = {1,2,3,4,5,6,7,8,9,10,12};
% 
% Control_ = 1;
% Autism_ = 1;
% 
% 
%     i1 = 4;
%     
%      Message = strcat('Now working on Autism:**', num2str(Autism_Components{i1}))
%         size(Autism_Components{i1})
%         
%         ICIndex = obj.result.foundComp(Autism_Components{i1},1);                     %# Read the first element (1,1)     
% for i = 2: 392               %# Loop from element 2 thru 799
%     ICIndex = [ICIndex obj.result.foundComp(Autism_Components{i1}, i)]       %# Add th eelement to ICIndex
% end
% 
% size(ICIndex)
% 
% 
% 
% 
% 
%  dirName = '/home/mri_s2/mas0047/gR_Data/Autism';
%    dirData = dir(dirName);      %# Get the data for the current directory
%    dirIndex = [dirData.isdir];  %# Find the index for directories
%    fileList = {dirData(~dirIndex).name}';  %'# Get a list of the files
%    if ~isempty(fileList)
%      fileList = cellfun(@(x) fullfile(dirName,x),...  %# Prepend path to files
%                         fileList,'UniformOutput',false);
%    end
%    subDirs = {dirData(dirIndex).name};  %# Get a list of the subdirectories
%   validIndex = ~ismember(subDirs,{'.','..'});  %# Find index of subdirectories
%   i = 0;                                             %#   that are not '.' or '..'
%   for iDir = find(validIndex)                  %# Loop over valid subdirectories
%     nextDir = fullfile(dirName,subDirs{iDir});
%     path = strcat(nextDir, '/filtered_func_data.ica/melodic_IC.nii')
%    
%     
%     if exist (path, 'file')
%         
%         
%         if i  == 0
%             a = load_nii(path);
%             size(a.img);
%           % disp(strcat('Current file: ', path, '*** IC Index: ', num2str(ICIndex(i+1)           
%           x = squeeze(a.img(:,:,:,ICIndex(i+1)));
%             x_vec=reshape(x,1,61*73*61);
%            
%         else
%             if (i < 392)
%                 
%                 
%             a = load_nii(path);
%             size(a.img);
%             information = strcat(path, '*** Added to the Vector')
%             
%            % disp(strcat('Current file: ', path, '*** IC Index: ', num2str(ICIndex(i+1))))
%             x = squeeze(a.img(:,:,:,ICIndex(i+1)));
%             y_vec=reshape(x,1,61*73*61);
%             x_vec = [x_vec; y_vec];
%             
%             else
%                 information = strcat(path, '*** Not Added to the Vector')
%             end
%             
%             
%         end
% 
%             
%             
%   
%     else
%         %warningMessage = sprintf('Warning: File Does Not Exist \n%s', path);
%        % uiwait(msgbox(warningMessage));
%     end
%         
%     
%     
%   i= i + 1
%   end
%   size(x_vec);
%   Aut_Vec = x_vec;
%   A = mean(x_vec);
%   
%     
%     
%     %for j1 = 1:3
% j1 = 3;
%     
%   Message = strcat('Now working on Control:**', num2str(Control_Components{j1}), '**and Autism:**', num2str(Autism_Components{i1}))
%   
%   disp('Trying to load Control Set now...')
%   load ('/home/mri_s2/mas0047/gR_Data/Control/gRSecondRun_C/task_result.mat'); %# Load task_result.mat
% %addpath('/home/mri_s2/mas0047/NIFTI_ANALYZE');
% ICIndex = obj.result.foundComp(Control_Components{j1},1);                     %# Read the first element (1,1)     
% for i = 2: 407               %# Loop from element 2 thru 799
%     ICIndex = [ICIndex obj.result.foundComp(Control_Components{j1}, i)]  ;       %# Add th eelement to ICIndex
% end
% 
% size(ICIndex)
%   
%  dirName = '/home/mri_s2/mas0047/gR_Data/Control';
%    dirData = dir(dirName);      %# Get the data for the current directory
%    dirIndex = [dirData.isdir];  %# Find the index for directories
%    fileList = {dirData(~dirIndex).name}';  %'# Get a list of the files
%    if ~isempty(fileList)
%      fileList = cellfun(@(x) fullfile(dirName,x),...  %# Prepend path to files
%                         fileList,'UniformOutput',false);
%    end
%    subDirs = {dirData(dirIndex).name};  %# Get a list of the subdirectories
%   validIndex = ~ismember(subDirs,{'.','..'});  %# Find index of subdirectories
%   i = 0;                                             %#   that are not '.' or '..'
%   for iDir = find(validIndex)                  %# Loop over valid subdirectories
%     nextDir = fullfile(dirName,subDirs{iDir});
%     path = strcat(nextDir, '/filtered_func_data.ica/melodic_IC.nii')
%    
%     
%     if exist (path, 'file')
%         
%         
%         if ( i < 407)
%             a = load_nii(path);
%             size(a.img);
%            % disp(strcat('Current file: ', path, '*** IC Index: ', num2str(ICIndex(i+1))))
%             x = squeeze(a.img(:,:,:,ICIndex(i+1)));
%             y_vec=reshape(x,1,61*73*61);
%            
%             if i == 0
%                  x_vec1 = [Aut_Vec; y_vec];
%                 C = y_vec;
%             else
%                  x_vec1 = [x_vec1; y_vec];
%                 C = [C; y_vec];
%             end
%             
%         else
%             disp(path, '*** Not Added to the Vector')
%             
%         end
%             
%   
%     else
%        % warningMessage = sprintf('Warning: File Does Not Exist \n%s', path);
%        % uiwait(msgbox(warningMessage));
%     end
%         
%      
%      
%    i= i + 1; 
%   end
%   
%    size(x_vec1)
%    Cont_Vec = C;
% C = mean(C);
% D = [A; C];
% 
%    % 
% % 
%    %IDX = kmeans(x_vec1, 2, 'start', D)
%    [IDX, C] = kmeans(x_vec1, 2, 'start', D);
%    figure
%    scatter(A, C)
%    %plot(x_vec1(IDX==1,1), x_vec1(IDX==1,2), 'r.', 'MarkerSize', 12)
%    %hold on
%    %plot(x_vec1(IDX==2,1), x_vec1(IDX==2,2), 'b.', 'MarkerSize', 12)
%    %plot(C(:,1), C(:,2), 'kx', 'MarkerSize', 12, 'LineWidth', 2)
%    %plot(C(:,1), C(:,2), 'ko', 'MarkerSize', 12, 'LineWidth', 2)
%    %legend('Cluster 1', 'Cluster 2', 'Centroids')
%    %hold off
%    
% %   
% %fname = strcat('/home/mri_s2/mas0047/gR_Data/KMeansData/kMeansDataComponent_', num2str(i1), '_', num2str(j1), '.mat')
% %CD = fname;
%  %  save (fname, 'IDX') 
% %disp(fname)
% %disp('Saved')
%         
%    % end
%     
%     
% %   
% %   
% %   
