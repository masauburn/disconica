function varargout = GetKMeansData(varargin)

addpath('/home/mas0047/NIfTI');
test_folder = '/home/mas0047/PTSD_ANALYSIS';
%put for loop here

destName = test_folder; %varargin{1}; %main directory with groups

px = '/gpfs01/home/mas0047/PTSD_ANALYSIS/PTSD/Melodic/gR/grout/compMaps/comp050.nii.gz';
nx = '/gpfs01/home/mas0047/PTSD_ANALYSIS/PCSP/Melodic/gR/grout/compMaps/comp062.nii.gz';
cx = '/gpfs01/home/mas0047/PTSD_ANALYSIS/CONT/Melodic/gR/grout/compMaps/comp052.nii.gz';

npx = '/gpfs01/home/mas0047/PTSD_ANALYSIS/PCSP/Melodic/gR/grout/compMaps/comp029.nii.gz';
cpx = '/gpfs01/home/mas0047/PTSD_ANALYSIS/CONT/Melodic/gR/grout/compMaps/comp050.nii.gz';

pnx = '/gpfs01/home/mas0047/PTSD_ANALYSIS/PTSD/Melodic/gR/grout/compMaps/comp57.nii.gz';
cnx = '/gpfs01/home/mas0047/PTSD_ANALYSIS/CONT/Melodic/gR/grout/compMaps/comp43.nii.gz';

pcx = '/gpfs01/home/mas0047/PTSD_ANALYSIS/PTSD/Melodic/gR/grout/compMaps/comp040.nii.gz';
ncx = '/gpfs01/home/mas0047/PTSD_ANALYSIS/PCSP/Melodic/gR/grout/compMaps/comp190.nii.gz';

% a = {px npx cpx};
% b = {pnx nx cnx};
% c = {pcx ncx cx};
anum = {50 29 50};
bnum = {43 62 57};
cnum = {52 190 40};


files = dir(destName)
dirFlags = [files.isdir]
subFolders = files(dirFlags)
folders = {};

%We are getting all group folders here
i = 1;
for k = 1 : length(subFolders)
    
    if (strcmp(subFolders(k).name, '.') == 0) && (strcmp(subFolders(k).name, '..') == 0)
        f1 = subFolders(k).name;
        folders = [folders f1];
        groups(i).name = subFolders(k).name;
        i=i+1;
        
    end
    
end


numGroups = length(groups);


taskResult = {};
resultFile = [];
%taskResult = [];

for i = 1:numGroups
    groups(i).resultFile = [];
    groups(i).melodicFile = [];
end

%parpool(4);

% this is being done for all groups
% we have task_result paths post-gRAICAR for all groups
for i = 1:numGroups
    resultFile = [destName '/' groups(i).name '/Melodic/gR/grout/task_result.mat'];
    
    if exist(resultFile, 'file') == 2
        taskResult = [taskResult resultFile];
        groups(i).resultFile = [destName '/' groups(i).name '/Melodic/gR/grout/task_result.mat']; 
        groups(i).melodicFile = [destName '/' groups(i).name '/Melodic/gR/grout/task_result.mat']; 
    
        
    end
    
end

% taskResult now has task_result paths for all groups

% the following loop unzips melodic files for all subjects in all groups

for i = 1:numGroups % total number of groups
    
   % reading subject list and unzipping all melodic files per subject in
   % ALL groups
   groups(i).melodicFiles = {};
   subList = [destName '/' groups(i).name '/Melodic/gR/subjects.list'];
   
    fid=fopen(subList);
        tline = fgetl(fid);
            while ischar(tline)
               
            
            if (~isempty(tline) & tline ~= -1)
             mFile = [destName '/' groups(i).name '/Melodic/gR/' tline '/filtered_func_data.ica/melodic_IC.nii.gz'];
             if exist(mFile, 'file')
                 %gunzip(mFile);
                 melodicFile = strcat(destName,'/',groups(i).name,'/Melodic/gR/',tline,'/filtered_func_data.ica/melodic_IC.nii');
                 if exist(melodicFile, 'file')
                     groups(i).melodicFiles = [groups(i).melodicFiles melodicFile];
                 end
                 
             end
            end
            
            tline = fgetl(fid);
            
           end
    fclose(fid);
    
end

% end unzipping melodic files

% Now get the best components

for i = 1: numGroups % this should match the total number of groups
    
    load(taskResult{1, i});
    A = obj.result.beta_rank_subjLoad;
    [M, I] = min(A);
    inds = find(A==M);
   
    groups(i).inds = inds;
    
    
end


for i = 1: numGroups   % should match the number of groups

    load(groups(i).resultFile);
    
    
        
        curComp = cnum{i}; % best component in group 1
        
        s = size(obj.result.foundComp(1,:));
        
        ICIndex = {};               %# Read the first element (1,1)     
        
        for i1 = 1: s(2)               %# Loop from subject 1 thru s(1)
            ICIndex = [ICIndex obj.result.foundComp(curComp, i1)];       %# Add th eelement to ICIndex
        end
        
        for i2 = 1: length(groups(i).melodicFiles)
            % files have been added because they exist
            
            a = load_nii(groups(i).melodicFiles{1,i2});
            ax=a.hdr.dime.dim(2);
            ay=a.hdr.dime.dim(3);
            az=a.hdr.dime.dim(4);
            x = squeeze(a.img(:,:,:,ICIndex{i2}));
             
            if i2 == 1
                
             x_vec=reshape(x,1,ax*ay*az);  
                
            else
                
             y_vec=reshape(x,1,ax*ay*az);
             x_vec = [x_vec; y_vec];
                
            end
            
            
            
         end
         
          size(x_vec);
          %groups(i).x_vec(j).ind = groups(i).inds(j);
          groups(i).x_vec(1).vec = x_vec;
          
          
    
    
end

for i = 1:numGroups
    
    subjects(i) = numel(groups(i).melodicFiles);
    
end


i = 1;
r = 1;
flag = 0;
result = [];

% Test Recursion




% End Test Recursion


    for j = 1:1
     
        i = 1;
        a(j).vec = groups(i).x_vec(j).vec;
        a(j).mean = mean(a(j).vec);
        next = i+1
        disp(groups(i).name);
     
     if (next <= numGroups)
        
         for k = 1:1
         
        disp(groups(next).name);
             
         b(k).vec = [a(j).vec; groups(next).x_vec(k).vec];
         b(k).mean = [a(j).mean; mean(groups(next).x_vec(k).vec)];
         
         next = next + 1;
         
          if (next <= numGroups && flag == 0)
            % 3 Groups  
            for l = 1:1
                disp(groups(next).name);
                z(l).vec = [b(k).vec; groups(next).x_vec(l).vec];
                z(l).mean = [b(k).mean; mean(groups(next).x_vec(l).vec)];
                
                result(r).numGroups = numGroups;
                result(r).subjects = subjects;
                result(r).vec = z(l).vec;
                result(r).mean = z(l).mean;
                result(r).groups(i).name = groups(i).name;
                %result(r).groups(i).currComp = groups(i).x_vec(j).ind;
                result(r).groups(i+1).name = groups(i+1).name;
                %result(r).groups(i+1).currComp = groups(i+1).x_vec(k).ind;
                result(r).groups(i+2).name = groups(i+2).name;
                %result(r).groups(i+2).currComp = groups(i+2).x_vec(l).ind;
              
              r = r + 1
              next = next - 1
              
                
            
            end
               
          else
              % 2 Groups
              
              flag = 1;
              
              %confMatrix = KMeans(numGroups, subjects, b(k).vec, b(k).mean);
              %result(r).confMatrix = confMatrix;
              result(r).numGroups = numGroups;
              result(r).subjects = subjects;
              result(r).vec = b(k).vec;
              result(r).mean = b(k).mean;
              result(r).groups(i).name = groups(i).name;
              result(r).groups(i).currComp = groups(i).x_vec(j).ind;
              result(r).groups(i+1).name = groups(i+1).name;
              result(r).groups(i+1).currComp = groups(i+1).x_vec(k).ind;
              r = r + 1
              next = next - 1
          end
          
         end
         
         
     end
         
     
     
        
    end
    
   groupSt = '';
   
   for i=1:numGroups
   
       
   end
   
   
    
    for res=1:numel(result)
       %
       [IDX, ctrs] = kmeans(result(res).vec, result(res).numGroups, 'start', result(res).mean);

        for i=1:numGroups
            g(i).positive = 0;
            g(i).negative = 0;

           if i == 1

               for j = 1:subjects(i)
                   if IDX(j) == i
                       g(i).positive = g(i).positive + 1;

                   else
                       g(i).negative = g(i).negative + 1;
                   end
               end
               g(i).purity = g(i).positive/subjects(i);
           else
               start = subjects(i-1) + 1;
               stop = subjects(i-1) + subjects(i);
               for j = start:stop
                   if IDX(j) == i
                       g(i).positive = g(i).positive + 1;

                   else
                       g(i).negative = g(i).negative + 1;
                   end
               end
               g(i).purity = g(i).positive/subjects(i);

           end


        end




       
       
       %
    end
    
          
          
         
        
        
end

function testRecursion = Recursiontest(groups, j)

for i = 1:numel(groups(j).inds)
   a(i).vec = groups(i).x_vec(j).vec;
   a(i).mean = mean(a(j).vec);
   j = j+1;
   if (j <= numGroups)
   a(i).vec = [a(i).vec; testRecursion(groups, j)];
       
   end
end

end
    
% k means
% idx
% subjects in each group
% subjects in group 1
% if idx 1.. subjects in group 1 == 1
% cluster 1 ++
% purity
% cluster 1 / subjects in group 1, if 100 %, stop
% subjects in group 2
% if idx subjects in group 2.. subjects in group 2 == 2
% cluster 2 ++

    

% 
% 
% Control_Components = {5,2,7,15};
% Autism_Components = {1,2,3,4,5,6,7,8,9,10,12, 22};
% 
% Control_ = 1;
% Autism_ = 12;
% 
% 
%     i1 = 12;
%     
%      Message = strcat('Now working on Autism:**', num2str(Autism_Components{i1}))
%         size(Autism_Components{i1})
%         
%         ICIndex = obj.result.foundComp(Autism_Components{i1},1);                     %# Read the first element (1,1)     
% for i = 2: 392               %# Loop from element 2 thru 799
%     ICIndex = [ICIndex obj.result.foundComp(Autism_Components{i1}, i)]       %# Add th eelement to ICIndex
% end
% 
% size(ICIndex)
% 
% 
% 
% 
% 
%  dirName = '/home/mri_s2/mas0047/ABIDE_DATA/gR_Data/Autism';
%    dirData = dir(dirName);      %# Get the data for the current directory
%    dirIndex = [dirData.isdir];  %# Find the index for directories
%    fileList = {dirData(~dirIndex).name}';  %'# Get a list of the files
%    if ~isempty(fileList)
%      fileList = cellfun(@(x) fullfile(dirName,x),...  %# Prepend path to files
%                         fileList,'UniformOutput',false);
%    end
%    subDirs = {dirData(dirIndex).name};  %# Get a list of the subdirectories
%   validIndex = ~ismember(subDirs,{'.','..'});  %# Find index of subdirectories
%   i = 0;                                             %#   that are not '.' or '..'
%   for iDir = find(validIndex)                  %# Loop over valid subdirectories
%     nextDir = fullfile(dirName,subDirs{iDir});
%     path = strcat(nextDir, '/filtered_func_data.ica/melodic_IC.nii')
%    
%     
%     if exist (path, 'file')
%         
%         
%         if i  == 0
%             a = load_nii(path);
%             size(a.img);
%           % disp(strcat('Current file: ', path, '*** IC Index: ', num2str(ICIndex(i+1)           
%           x = squeeze(a.img(:,:,:,ICIndex(i+1)));
%             x_vec=reshape(x,1,61*73*61);
%        
%         elseif (i > 0 && i < 392) % && i ~= 240) % i = 240 added for data point removal
%                 
%                 
%             a = load_nii(path);
%             size(a.img);
%             information = strcat(path, '*** Added to the Vector')
%             
%            % disp(strcat('Current file: ', path, '*** IC Index: ', num2str(ICIndex(i+1))))
%             x = squeeze(a.img(:,:,:,ICIndex(i+1)));
%             y_vec=reshape(x,1,61*73*61);
%             x_vec = [x_vec; y_vec];
%             
%             else
%                 information = strcat(path, '*** Not Added to the Vector')
%             
%             
%             
%         end
% 
%             
%             
%   
%     else
%         %warningMessage = sprintf('Warning: File Does Not Exist \n%s', path);
%        % uiwait(msgbox(warningMessage));
%     end
%         
%     
%     
%   i= i + 1
%   end
%   size(x_vec);
%   Aut_Vec = x_vec;
%   A = mean(x_vec);
%   D1 = pdist(Aut_Vec, 'euclidean');
%     
%     
%     %for j1 = 1:3
% j1 = 3;
%     
%   Message = strcat('Now working on Control:**', num2str(Control_Components{j1}), '**and Autism:**', num2str(Autism_Components{i1}))
%   
%   disp('Trying to load Control Set now...')
%   load ('/home/mri_s2/mas0047/ABIDE_DATA/gR_Data/Control/gRSecondRun_C/task_result.mat'); %# Load task_result.mat
% %addpath('/home/mri_s2/mas0047/NIFTI_ANALYZE');
% ICIndex = obj.result.foundComp(Control_Components{j1},1);                     %# Read the first element (1,1)     
% for i = 2: 407               %# Loop from element 393 thru 799
%     ICIndex = [ICIndex obj.result.foundComp(Control_Components{j1}, i)]  ;       %# Add th eelement to ICIndex
% end
% 
% size(ICIndex)
%   
%  dirName = '/home/mri_s2/mas0047/ABIDE_DATA/gR_Data/Control';
%    dirData = dir(dirName);      %# Get the data for the current directory
%    dirIndex = [dirData.isdir];  %# Find the index for directories
%    fileList = {dirData(~dirIndex).name}';  %'# Get a list of the files
%    if ~isempty(fileList)
%      fileList = cellfun(@(x) fullfile(dirName,x),...  %# Prepend path to files
%                         fileList,'UniformOutput',false);
%    end
%    subDirs = {dirData(dirIndex).name};  %# Get a list of the subdirectories
%   validIndex = ~ismember(subDirs,{'.','..'});  %# Find index of subdirectories
%   i = 0;                                             %#   that are not '.' or '..'
%   for iDir = find(validIndex)                  %# Loop over valid subdirectories
%     nextDir = fullfile(dirName,subDirs{iDir});
%     path = strcat(nextDir, '/filtered_func_data.ica/melodic_IC.nii')
%    
%     
%     if exist (path, 'file')
%         
%         
%         if ( i < 407)
%             a = load_nii(path);
%             size(a.img);
%            % disp(strcat('Current file: ', path, '*** IC Index: ', num2str(ICIndex(i+1))))
%             x = squeeze(a.img(:,:,:,ICIndex(i+1)));
%             y_vec=reshape(x,1,61*73*61);
%            
%             if i == 0
%                  x_vec1 = [Aut_Vec; y_vec];
%                 C = y_vec;
%             else
%                  x_vec1 = [x_vec1; y_vec];
%                 C = [C; y_vec];
%             end
%             
%         else
%             disp(path, '*** Not Added to the Vector')
%             
%         end
%             
%   
%     else
%        % warningMessage = sprintf('Warning: File Does Not Exist \n%s', path);
%        % uiwait(msgbox(warningMessage));
%     end
%         
%      
%      
%    i= i + 1; 
%   end
%   
%    size(x_vec1);
%    A = x_vec1;
%    %save combinedAC.mat x_vec1 
%  Cont_Vec = C;
%  C = mean(C);
%  D = [A; C];
%  D2 = pdist(Cont_Vec, 'euclidean');
% % 
% % %MDS Start
% % 
% % YA = mdscale(D1, 2);
% % YC = mdscale(D2, 2);
% % YT = [YA; YC];
% % %save combinedACMDS2D.mat YT
% % 
% % M1 = mean(YA);
% % M2 = mean(YC);
% % D3 = [M1; M2];
% % size(YT);
% % %MDS End
% % save('/home/mri_s2/mas0047/test.mat', 'x_vec1')
% %    % 
% % % 
% %    % IDX = kmeans(x_vec1, 2, 'start', D)
% %     
% %     %P = pdist(x_vec1)
% %     
% %     
% %    [IDX, ctrs] = kmeans(x_vec1, 2, 'start', D)
% %    % [silh2,h] = silhouette(x_vec1, IDX)
% %    %    IDX1 = x_vec1(IDX == 1, :);
% % %    IDX2 = x_vec1(IDX == 2, :);
% % %    
% % %    figure
% % %    axes
% % %    
% % %   plot(IDX1(:, [3, 4]), '*'); hold all
% % %   plot(IDX2(:, [3, 4]), '*')
% % %   
% % %   xlabel('Distance')
% % %   ylabel('Distance')
% %    
% %   % [IDX, ctrs] = kmeans(YT, 2, 'start', D3)
% %  % [IDX, ctrs] = kmeans(YT, 2, 'start', D3)
% %   save('/home/mri_s2/mas0047/idx.mat', 'IDX')
% % 
% % %   figure
% % %    
% % %    figure
% % %    scatter(A, C)
% % %    plot(x_vec1(IDX==1,1), x_vec1(IDX==1,2), 'r.', 'MarkerSize', 12)
% % %    hold on
% % %    plot(x_vec1(IDX==2,1), x_vec1(IDX==2,2), 'b.', 'MarkerSize', 12)
% % %    plot(ctrs(:,1), ctrs(:,2), 'kx', 'MarkerSize', 12, 'LineWidth', 2)
% % %    plot(ctrs(:,1), ctrs(:,2), 'ko', 'MarkerSize', 12, 'LineWidth', 2)
% % %    legend('Cluster 1', 'Cluster 2', 'Centroids')
% % %    hold off
% % %     plot(YT(IDX==1,1), YT(IDX==1,2), 'r+', 'MarkerSize', 8)
% % %    hold on
% % %    plot(YT(IDX==2,1), YT(IDX==2,2), 'bo', 'MarkerSize', 8)
% % %   plot(ctrs(:,1), ctrs(:,2), 'kx', 'MarkerSize', 10, 'LineWidth', 3)
% % %   plot(ctrs(:,1), ctrs(:,2), 'ko', 'MarkerSize', 10, 'LineWidth', 3)
% % %    legend('Classified as Autism', 'Classified as Control', 'Centroids')
% % %    xlabel('Inter-Subject Distance')
% % %    ylabel('Inter-Subject Distance')
% % %    grid on
% % %    hold off
% % %   
% % %fname = strcat('/home/mri_s2/mas0047/gR_Data/KMeansData/kMeansDataComponent_', num2str(i1), '_', num2str(j1), '.mat')
% % %CD = fname;
% %  %  save (fname, 'IDX') 
% % %disp(fname)
% % %disp('Saved')
% %         
% %    % end
% %     
% %     
% % %   
% % %   
% % %   
