function varargout = disconica(varargin)
% DISCONICA MATLAB code for disconica.fig
%      DISCONICA, by itself, creates a new DISCONICA or raises the existing
%      singleton*.
%
%      H = DISCONICA returns the handle to a new DISCONICA or the handle to
%      the existing singleton*.
%
%      DISCONICA('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DISCONICA.M with the given input arguments.
%
%      DISCONICA('Property','Value',...) creates a new DISCONICA or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before disconica_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to disconica_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help disconica

% Last Modified by GUIDE v2.5 21-Jan-2018 01:48:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @disconica_OpeningFcn, ...
                   'gui_OutputFcn',  @disconica_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before disconica is made visible.
function disconica_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to disconica (see VARARGIN)

% Choose default command line output for disconica
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
% Options Panel Default Settings
set(handles.pnlOptions,'visible','off')
set(handles.cbRemoveFirst,'value',0)
set(handles.tbTimePoints,'string','')
set(handles.cbSliceTiming,'value',1)
set(handles.tbSliceNumber,'string','33')


% UIWAIT makes disconica wait for user response (see UIRESUME)
% uiwait(handles.disconica);
%set(handles.ok, 'Enable', 'off');
global folder_name;



% --- Outputs from this function are returned to the command line.
function varargout = disconica_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function tbdcmLocation_Callback(hObject, eventdata, handles)
% hObject    handle to tbdcmLocation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tbdcmLocation as text
%        str2double(get(hObject,'String')) returns contents of tbdcmLocation as a double


% --- Executes during object creation, after setting all properties.
function tbdcmLocation_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tbdcmLocation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in browse.
function browse_Callback(hObject, eventdata, handles)
% hObject    handle to browse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

folder_name = uigetdir();

%Check to see if all folders have dcm files, should be equal in number
%Same number of time points in one analysis

set(handles.tbdcmLocation, 'string', folder_name);

% --- Executes on button press in ok.
function ok_Callback(hObject, eventdata, handles)
% hObject    handle to ok (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% create directory 'FunRaw' or 'FunImg' 
% Move all subject folders there.
% Call DPARSF with location
% DPARSF stores results under FunRaw NormalizaedDetrended\Subject...
% Go to each subject, convert to 4D, rename based upon subject
% move the nii s to outside FunRaw or FunImg


%
formatOut = 'mm_dd_yy';
log_fn = ['disconica_log_' datestr(now,formatOut) '.log'];
soft = mfilename('fullpath');
[pathstr,name,ext] = fileparts(soft);

formatOut = 'mm_dd_yy';
log_fn = ['disconica_log_' datestr(now,formatOut) '.log'];
log_loc = fullfile(pathstr, 'logs');
data_loc = fullfile(pathstr, 'data');
      

if exist(log_loc, 'dir') ~= 7
    mkdir(log_loc);
end

log_fn = fullfile(log_loc, log_fn);

if exist(log_fn, 'file') == 2
    open = 'a';
else
    open = 'w';
end



%
log = sprintf('%s %s', datestr(now), 'DisConICA Started')
log = sprintf('%s\n%s %s', log, datestr(now), 'Setting up initialization parameters')

fileID = fopen(log_fn, open);
fprintf(fileID, log);
fclose(fileID);

try
    
    handles = guidata(hObject);
    parameters = collect_Inputs(handles);
    parameters.folder_name = get(handles.tbdcmLocation, 'string');
    
    files = dir(parameters.folder_name);
    dirFlags = [files.isdir];
    subFolders = files(dirFlags);
    folders = {};

    % We are getting all group folders here
    
    i = 1;
    for k = 1 : length(subFolders)

        if (strcmp(subFolders(k).name, '.') == 0) && (strcmp(subFolders(k).name, '..') == 0)
            f1 = subFolders(k).name;
            folders = [folders f1];
            groups(i).name = subFolders(k).name;
            i=i+1;

        end

    end


    numGroups = length(groups);
    
    
    parameters.TR = str2double(get(handles.tbTR, 'string'));
    
    if parameters.TR == ''
        h = msgbox('TR cannot be empty.','DisConICA')
    elseif numGroups < 2 | numGroups > 3
        h = msgbox('DisConICA currently is available for 2 or 3 groups.','DisConICA')
    else
    
        
            
            parameters.fsl_string = get(handles.tbFSL, 'string');
            parameters.gRProc = 2;
            value = get(handles.cbBM, 'Value');

            if value == 1
                if exist(data_loc, 'dir') ~= 7
                    mkdir(data_loc);
                end

                param_fn = 'parameters.mat';
                param_loc = fullfile(data_loc, param_fn);
                save (param_loc, 'parameters');
                h = msgbox('For batch mode operation, please follow instructions for further analysis','DisConICA')

            else
                
                h = msgbox('Please check Batch Mode check box and try again','DisConICA')
%             % Start Create FSL Environment
%                 parameters.path_name = '';
%                 f_string = strsplit(parameters.fsl_string, ';')
%         %         f_1 = 'module load fsl/6.0' 
%         %         f_2 = 'source /tools/fsl-6.0/etc/fslconf/fsl.sh'
% 
%                 for i = 1:length(f_string)
%                     status = system(char(f_string(i)))
% 
%                 end
% 
%                 error = 0;
% 
%                 log = sprintf('%s\n%s %s', log, datestr(now), 'Setting up FSL initialization');
%             %     status = system('module load fsl/6.0')
%             %     status = system('source /tools/fsl-6.0/etc/fslconf/fsl.sh')
%             %     
%                 % End Create FSL Environment
% 
% 
%                 % Call PrepEnvironment
%                 disp('Prep Environment Started')
%                 tic
%                 if (error == 0)
% 
%                     log = sprintf('%s\n%s %s', log, datestr(now), 'Calling PrepEnvironment');   
%                     [error, groups] = PrepEnvironment(parameters);
%                     log = sprintf('%s\n%s %s %s', log, datestr(now), 'In DisConICA, PrepEnvironment Completed, Error = ', error);
% 
%                 end
%                 disp('Prep Environment Ended')
%                 toc
%                 %
%                 disp('DPARSF Started')
%                 tic
%                 if (error == 0)
% 
%                     log = sprintf('%s\n%s %s', log, datestr(now), 'Calling PrepRunDPARSF');   
%                     [error, groups] = PrepRunDPARSF(parameters, groups);
%                     log = sprintf('%s\n%s %s %s', log, datestr(now), 'In DisConICA, PrepRunDPARSF Completed, Error = ', error);
% 
%                 end
%                 disp('DPARSF Ended')
%                 toc
%                 
%                 disp('Melodic Started')
%                 tic
%                 
%                 if (error == 0)
% 
%                     log = sprintf('%s\n%s %s', log, datestr(now), 'Calling PrepRunMELODIC');   
%                     [error, groups] = PrepRunMELODIC(parameters, groups);
%                     log = sprintf('%s\n%s %s %s', log, datestr(now), 'In DisConICA, PrepRunMELODIC Completed, Error = ', error);
% 
%                 end
%                 disp('Melodic Ended')
%                 toc
%                 
%                 disp('gRAICAR Started')
%                 tic
%                 if (error == 0)
% 
%                     log = sprintf('%s\n%s %s', log, datestr(now), 'Calling PrepRungRAICAR');   
%                     error = PrepRungRAICAR_T(parameters);
%                     log = sprintf('%s\n%s %s %s', log, datestr(now), 'In DisConICA, PrepRungRAICAR Completed, Error = ', error);
% 
%                 end
%                 disp('gRAICAR Ended')
%                 toc
%                 
%                 disp('Clustering Started')
%                 tic
%                 
%                 if (error == 0)
% 
%                     log = sprintf('%s\n%s %s', log, datestr(now), 'Calling Setup Clustering');
%                     error = SetupClustering(parameters);
%                     log = sprintf('%s\n%s %s %s', log, datestr(now), 'In DisConICA, SetupClustering Completed, Error = ', error);
% 
%                 end
%                 
%                 disp('Clustering Ended')
%                 toc
%                 
%                 if (error == 1)
%                     h = msgbox('DisConICA did not complete successfully.','DisConICA', 'error');
%                     fileID = fopen(log_fn, open);
%                     fprintf(fileID, log);
%                     fclose(fileID);
%                 else
%                     h = msgbox('DisConICA has completed processing successfully.','DisConICA');
%                 end

            end
            
    end

catch e
    
    h = msgbox('DisConICA did not complete successfully.','DisConICA', 'error')
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'Main Error, Identifier: ', e.identifier); 
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'Main Error, Message: ', e.message);
    fileID = fopen(log_fn, open);
    fprintf(fileID, log);
    fclose(fileID);

end
        
    




% --- Executes on button press in cancel.
function cancel_Callback(hObject, eventdata, handles)
% hObject    handle to cancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Construct a questdlg with three options
choice = questdlg('Are you sure you want to exit?', ...
	'Close Menu', ...
	'Yes','No','No');
% Handle response
switch choice
    case 'Yes'
        close all;
    case 'No'
        disp('Thank You');
end



function tbTR_Callback(hObject, eventdata, handles)
% hObject    handle to tbTR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tbTR as text
%        str2double(get(hObject,'String')) returns contents of tbTR as a double


% --- Executes during object creation, after setting all properties.
function tbTR_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tbTR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in radiobuttonDCM.
function radiobuttonDCM_Callback(hObject, eventdata, handles)
% hObject    handle to radiobuttonDCM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobuttonDCM


% --- Executes on button press in radiobuttonIMG.
function radiobuttonIMG_Callback(hObject, eventdata, handles)
% hObject    handle to radiobuttonIMG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobuttonIMG


% --- Executes on button press in cbRemoveFirst.
function cbRemoveFirst_Callback(hObject, eventdata, handles)
% hObject    handle to cbRemoveFirst (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbRemoveFirst



function tbTimePoints_Callback(hObject, eventdata, handles)
% hObject    handle to tbTimePoints (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tbTimePoints as text
%        str2double(get(hObject,'String')) returns contents of tbTimePoints as a double


% --- Executes during object creation, after setting all properties.
function tbTimePoints_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tbTimePoints (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in cbSliceTiming.
function cbSliceTiming_Callback(hObject, eventdata, handles)
% hObject    handle to cbSliceTiming (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbSliceTiming



function tbSliceNumber_Callback(hObject, eventdata, handles)
% hObject    handle to tbSliceNumber (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tbSliceNumber as text
%        str2double(get(hObject,'String')) returns contents of tbSliceNumber as a double


% --- Executes during object creation, after setting all properties.
function tbSliceNumber_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tbSliceNumber (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tbSliceOrder_Callback(hObject, eventdata, handles)
% hObject    handle to tbSliceOrder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tbSliceOrder as text
%        str2double(get(hObject,'String')) returns contents of tbSliceOrder as a double


% --- Executes during object creation, after setting all properties.
function tbSliceOrder_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tbSliceOrder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tbReferenceSlice_Callback(hObject, eventdata, handles)
% hObject    handle to tbReferenceSlice (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tbReferenceSlice as text
%        str2double(get(hObject,'String')) returns contents of tbReferenceSlice as a double


% --- Executes during object creation, after setting all properties.
function tbReferenceSlice_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tbReferenceSlice (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in cbRealign.
function cbRealign_Callback(hObject, eventdata, handles)
% hObject    handle to cbRealign (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbRealign


% --- Executes on button press in cbNormalize.
function cbNormalize_Callback(hObject, eventdata, handles)
% hObject    handle to cbNormalize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbNormalize



function tbBoundingBox_Callback(hObject, eventdata, handles)
% hObject    handle to tbBoundingBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tbBoundingBox as text
%        str2double(get(hObject,'String')) returns contents of tbBoundingBox as a double


% --- Executes during object creation, after setting all properties.
function tbBoundingBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tbBoundingBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tbVoxelSize_Callback(hObject, eventdata, handles)
% hObject    handle to tbVoxelSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tbVoxelSize as text
%        str2double(get(hObject,'String')) returns contents of tbVoxelSize as a double


% --- Executes during object creation, after setting all properties.
function tbVoxelSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tbVoxelSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in cbSmooth.
function cbSmooth_Callback(hObject, eventdata, handles)
% hObject    handle to cbSmooth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbSmooth



function tbFWHM_Callback(hObject, eventdata, handles)
% hObject    handle to tbFWHM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tbFWHM as text
%        str2double(get(hObject,'String')) returns contents of tbFWHM as a double


% --- Executes during object creation, after setting all properties.
function tbFWHM_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tbFWHM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in cbDetrend.
function cbDetrend_Callback(hObject, eventdata, handles)
% hObject    handle to cbDetrend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbDetrend


% --- Executes on button press in cbFilter.
function cbFilter_Callback(hObject, eventdata, handles)
% hObject    handle to cbFilter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbFilter



function tbFilterTo_Callback(hObject, eventdata, handles)
% hObject    handle to tbFilterTo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tbFilterTo as text
%        str2double(get(hObject,'String')) returns contents of tbFilterTo as a double


% --- Executes during object creation, after setting all properties.
function tbFilterTo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tbFilterTo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tbFilterFrom_Callback(hObject, eventdata, handles)
% hObject    handle to tbFilterFrom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tbFilterFrom as text
%        str2double(get(hObject,'String')) returns contents of tbFilterFrom as a double


% --- Executes during object creation, after setting all properties.
function tbFilterFrom_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tbFilterFrom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tbNumProcessors_Callback(hObject, eventdata, handles)
% hObject    handle to tbNumProcessors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tbNumProcessors as text
%        str2double(get(hObject,'String')) returns contents of tbNumProcessors as a double


% --- Executes during object creation, after setting all properties.
function tbNumProcessors_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tbNumProcessors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
%function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
%function Untitled_2_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in btnShowOpt.
function btnShowOpt_Callback(hObject, eventdata, handles)
% hObject    handle to btnShowOpt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.pnlOptions,'visible','on')



% --- Executes on button press in btnHideOpt.
function btnHideOpt_Callback(hObject, eventdata, handles)
% hObject    handle to btnHideOpt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.pnlOptions,'visible','off')

function [parameter] = collect_Inputs(handles)
% hObject    handle to btnHideOpt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
cbStatus = get(handles.cbRemoveFirst,'Value');
if (cbStatus == 1)
    parameter.IsRemoveFirstTimePoints = 1;
    parameter.RemoveFirstTimePoints = get(handles.tbTimePoints,'String');
    if(parameter.RemoveFirstTimePoints == '')
        parameter.RemoveFirstTimePoints = 10;
    end
else
    parameter.IsRemoveFirstTimePoints = 0;
    parameter.RemoveFirstTimePoints = '';
    
        
end    

cbStatus = get(handles.cbSliceTiming,'Value');
if (cbStatus == 1)
    parameter.IsSliceTiming = 1;
    parameter.SliceNumber = get(handles.tbSliceNumber, 'String');
    parameter.SliceOrder = get(handles.tbSliceOrder, 'String');
    parameter.ReferenceSlice = get(handles.tbReferenceSlice, 'String');
    if (isempty(parameter.SliceNumber) == 1)
        parameter.SliceNumber = 33;
    end
    if (isempty(parameter.SliceOrder) == 1)
        parameter.SliceOrder = '[1:2:33,2:2:32]';
    end
    if (isempty(parameter.ReferenceSlice) == 1)
        parameter.ReferenceSlice = 33;
    end
else
    parameter.IsSliceTiming = 0;
    parameter.SliceOrder = '';
    parameter.ReferenceSlice = '';
    
    
end 
  
cbStatus = get(handles.cbRealign,'Value');
if(cbStatus == 1)
    parameter.IsRealign = 1;
else
    parameter.IsRealign = 0;
    
end

cbStatus = get(handles.cbNormalize,'Value');
if (cbStatus == 1)
   parameter.IsNormalize = 1; 
   parameter.BoundingBox = get(handles.tbBoundingBox,'String'); 
   parameter.VoxSize = get(handles.tbVoxelSize, 'String');
   if (isempty(parameter.BoundingBox) == 1)
       parameter.BoundingBox = '[-90 -126 -72;90 90 108]';
   end
   if(isempty(parameter.VoxSize) == 1)
       parameter.VoxSize = '[2 2 2]';
   end
   
else
    parameter.IsNormalize = 0;
    parameter.BoundingBox = ''; 
    parameter.VoxSize = '';
   
    
end

cbStatus = get(handles.cbSmooth,'Value');
if (cbStatus == 1)
    parameter.IsSmooth = 1;
    parameter.FWHM = get(handles.tbFWHM,'String'); 
    if(isempty(parameter.FWHM) == 1)
        parameter.FWHM = '[4 4 4]';
    end
    
else
    parameter.IsSmooth = 0;
    parameter.FWHM = '';
end

cbStatus = get(handles.cbDetrend,'Value');

if(cbStatus == 1)
    parameter.IsDetrend = 1;
else
    parameter.IsDetrend = 0;
    
end

cbStatus = get(handles.cbFilter,'Value');
if(cbStatus == 1)
    parameter.IsFilter = 1;
    parameter.AHighPass_LowCutoff = get(handles.tbFilterFrom, 'String');
    parameter.ALowPass_HighCutoff = get(handles.tbFilterTo, 'String');
    if(isempty(parameter.AHighPass_LowCutoff) == 1)
        parameter.AHighPass_LowCutoff = 0.01;
    end
    if(isempty(parameter.ALowPass_HighCutoff) == 1)
        parameter.ALowPass_HighCutoff = 0.1;
    end
    
else
     parameter.IsFilter = 0;
     parameter.AHighPass_LowCutoff = '';
     parameter.ALowPass_HighCutoff = '';
    
     
end

parameter.NoProcessors = 2;



function tbFSL_Callback(hObject, eventdata, handles)
% hObject    handle to tbFSL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tbFSL as text
%        str2double(get(hObject,'String')) returns contents of tbFSL as a double


% --- Executes during object creation, after setting all properties.
function tbFSL_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tbFSL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in cbBM.
function cbBM_Callback(hObject, eventdata, handles)
% hObject    handle to cbBM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbBM
