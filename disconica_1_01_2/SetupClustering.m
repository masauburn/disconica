function error = SetupClustering(parameters)

%

dfpath = parameters.folder_name;
formatOut = 'mm_dd_yy';
log_fn = ['disconica_log_' datestr(now,formatOut) '.log'];
soft = mfilename('fullpath');
[pathstr,name,ext] = fileparts(soft);

% if (~isempty(parameters.path_name))
%     pathstr = parameters.path_name
% end

addpath(pathstr);

%dfpath = parameters.folder_name;

log_loc = fullfile(pathstr, 'logs');

if exist(log_loc, 'dir') ~= 7
    mkdir(log_loc);
end

log_fn = fullfile(log_loc, log_fn);

if exist(log_fn, 'file') == 2
    open = 'w';
else
    open = 'a';
end

res_str = '';
res_fn = ['disconica_results_' datestr(now,formatOut) '.log'];
res_fn = fullfile(log_loc, res_fn);
if exist(res_fn, 'file') == 2
    open_res = 'w';
else
    open_res = 'a';
end
%
log = sprintf('%s %s', datestr(now), 'Setup Clustering Started');
log = sprintf('%s\n%s %s', log, datestr(now), 'Setting up initialization parameters');



fileID = fopen(log_fn, open);
fprintf(fileID, log);
fclose(fileID);
error = 0;

try
    
        folder_name = dfpath;
        %put for loop here
        
        destName = folder_name; %main directory with groups
        log = sprintf('%s\n%s %s %s', log, datestr(now), 'destName: ', destName);
        
        files = dir(destName)
        dirFlags = [files.isdir]
        subFolders = files(dirFlags)
        folders = {};
        
        %We are getting all group folders here
        log = sprintf('%s\n%s %s', log, datestr(now), 'Getting group folders: ');
        i = 1;
        for k = 1 : length(subFolders)
        
        if (strcmp(subFolders(k).name, '.') == 0) && (strcmp(subFolders(k).name, '..') == 0)
            f1 = subFolders(k).name;
            folders = [folders f1];
            groups(i).name = subFolders(k).name;
            i=i+1;
            
        end
        
        end
        
        
        numGroups = length(groups);
        
        
        %taskResult = {};
        resultFile = [];
        %taskResult = [];
        
        for i = 1:numGroups
            groups(i).resultFile = [];
            groups(i).melodicFile = [];
            groups(i).taskResult = [];
        end
        
        
        
        %parpool(4);
        
        % this is being done for all groups
        % we have task_result paths post-gRAICAR for all groups
        for i = 1:numGroups
            resultFile = [destName '/' groups(i).name '/Analysis/gR/gR_Results/task_result.mat'];
            
            if exist(resultFile, 'file') == 2
                groups(i).taskResult = resultFile;
                groups(i).resultFile = [destName '/' groups(i).name '/Analysis/gR/gR_Results/task_result.mat'];
                groups(i).melodicFile = [destName '/' groups(i).name '/Analysis/gR/gR_Results/task_result.mat'];
                
                
            end
            
        end
        
        for i = 1:numGroups
            log = sprintf('%s\n%s %s %s', log, datestr(now), 'group name: ', groups(i).name);
            log = sprintf('%s\n%s %s %s', log, datestr(now), 'result file: ', groups(i).resultFile);
            log = sprintf('%s\n%s %s %s', log, datestr(now), 'melodic name: ', groups(i).melodicFile);
        end
        
        % taskResult now has task_result paths for all groups
        
        % the following loop unzips melodic files for all subjects in all groups
        log = sprintf('%s\n%s %s', log, datestr(now), 'Unziping melodic files: ');
        subList = [];
        for i = 1:numGroups % total number of groups
            
            % reading subject list and unzipping all melodic files per subject in
            % ALL groups
            groups(i).melodicFiles = {};
            subList = [destName '/' groups(i).name '/Analysis/gR/subjects.list'];
            
            fid=fopen(subList);
            tline = fgetl(fid);
            while ischar(tline)
                
                log = sprintf('%s\n%s %s %s', log, datestr(now), 'Tline: ', tline);
                
                if (~isempty(tline) & tline ~= -1)
                    
                    log = sprintf('%s\n%s %s %s', log, datestr(now), 'Tline exists: ', tline);
                    mFile = [destName '/' groups(i).name '/Analysis/gR/' tline '/filtered_func_data.ica/melodic_IC.nii.gz'];
                    melodicFile = strcat(destName,'/',groups(i).name,'/Analysis/gR/',tline,'/filtered_func_data.ica/melodic_IC.nii.gz');
                    log = sprintf('%s\n%s %s %s', log, datestr(now), 'group name: ', groups(i).name);
                    log = sprintf('%s\n%s %s %s', log, datestr(now), 'Unzipping melodic file: ', melodicFile);
                    
%                     if ~exist(melodicFile, 'file')
%                         gunzip(mFile);
%                         log = sprintf('%s\n%s %s %s', log, datestr(now), 'Unzipped melodic file: ', groups(i).melodicFile);
%                         
%                     end
                    
                    if exist(melodicFile, 'file')
                        groups(i).melodicFiles = [groups(i).melodicFiles melodicFile];
                        log = sprintf('%s\n%s %s %s', log, datestr(now), 'melodic file exists: ', groups(i).melodicFile);
                    end
                    
                end
                
                tline = fgetl(fid);
                
            end
            fclose(fid);
            
        end
        
        % end unzipping melodic files
        
        % Now get the best components
        log = sprintf('%s\n%s %s', log, datestr(now), 'Getting best components:');
        for i = 1: numGroups % this should match the total number of groups
            
            load(groups(i).taskResult);
            A = obj.result.beta_rank_subjLoad;
            [M, I] = min(A);
            inds = find(A==M);
            
            groups(i).inds = inds(1);
            %numel(groups(i).inds);
            
            
        end
        
        log = sprintf('%s\n%s %s', log, datestr(now), 'Setting Indices:');
        
        [error, groups, max] = SetupX_Vec(groups, pathstr);
        
        
        log = sprintf('%s\n%s %s', log, datestr(now), 'Setting subject melodic files:');
        
        for i = 1:numGroups
            
            subjects(i) = numel(groups(i).melodicFiles);
            
        end
        
        %max = RunClustering(groups, pathstr);
        
        log = sprintf('%s\n%s %s', log, datestr(now), 'Getting component paths:');
        
        res_str = [res_str, datestr(now), '\n'];
        res_str = [res_str, 'DisConICA v 1.01.02 - Batch Mode', '\n'];
        res_str = [res_str, 'Starting Folder: ', parameters.folder_name, '\n'];
        
        for i=1:numGroups
            
            groups(i).compMaps = [destName '/' groups(i).name '/Analysis/gR/gR_Results/compMaps'];
            groups(i).maxComp = max.groups(i).currComp;
            groups(i).compIndex = max.groups(i).currComp;
            groups(i).currcompvec = max.groups(i).currcompvec;
            append = '';
            if (groups(i).maxComp < 10)
                append = '00';
            elseif (groups(i).maxComp > 9 && groups(i).maxComp < 100)
                append = '0';
            end
            
            t = ['comp' append int2str(groups(i).maxComp) '.nii.gz'];
            groups(i).maxComp = ['comp' append int2str(groups(i).maxComp) '.nii.gz'];
            
            groups(i).maxCompPath = [groups(i).compMaps '/' groups(i).maxComp]
            
            if ~exist(groups(i).maxCompPath, 'file')
                groups(i).maxCompPath = [groups(i).compMaps '/' t];
                
                if exist(groups(i).maxCompPath, 'file')
                    %gunzip(groups(i).maxCompPath);
                    groups(i).maxCompPath = [groups(i).compMaps '/' groups(i).maxComp];
                end
                
            end
            
            if i == 1
                  res_str = [res_str,  sprintf('Highest cluster purity: %f \n', max.maxval)];
                  res_str = [res_str, sprintf('Component %d (%s) from %s group was found to have the highest cluster purity with \n', groups(i).compIndex, groups(i).maxCompPath, groups(i).name)];
            else
                  res_str = [res_str, sprintf('Component %d (%s) from %s group \n', groups(i).compIndex, groups(i).maxCompPath, groups(i).name)];
            end
            
        end
        
        % 2 Groups
        
        if (numGroups == 2)
            
            gcc(1).maxComp = groups(1).maxCompPath;
            gcc(1).compMaps = groups(1+1).compMaps;

            gcc(2).maxComp = groups(1+1).maxCompPath;
            gcc(2).compMaps = groups(1).compMaps;

            for i = 1:numGroups
                [index path P R] = GetCorrCoeff(gcc(i), groups(i).ax, groups(i).ay, groups(i).az);
                gcc(i).index = index;
                
                if i == 1
                   res_str = [res_str, sprintf('Component %d from %s had the highest spatial correlation (%f) with component (%s) from %s group \n', groups(1).compIndex, groups(1).name, R, path, groups(2).name)];
                else
                   res_str = [res_str, sprintf('Component %d from %s had the highest spatial correlation (%f) with component (%s) from %s group \n', groups(2).compIndex, groups(2).name, R, path, groups(1).name)];
                end
            end

            % get x_vecs for these two
            
           for i = 1:numGroups % 2
                
                [error, opp_vec] = SetupOpp_Vec(groups(i).resultFile, groups(i).melodicFiles, gcc(i).index, pathstr);
                groups(i).opp_vec = opp_vec;
                

            end

            % Put through K Means
            for i = 1:numGroups
                
                opp(i).vec = [groups(i).currcompvec; groups(i).opp_vec];
                opp(i).mean = [mean(groups(i).currcompvec); mean(groups(i).opp_vec)];
                if i == 2
                    subjects = fliplr(subjects);
                    g = KMeans(numGroups, subjects, opp(i).vec, opp(i).mean);
                else
                    g = KMeans(numGroups, subjects, opp(i).vec, opp(i).mean);
                end
                opp(i).g = g;
                if i == 1
                    res_str = [res_str, sprintf('Cluster purity when combining the highly reproducible component from %s group with its spatial counterpart in %s group: \n', groups(i).name, groups(i+1).name)];
                else
                    res_str = [res_str, sprintf('Cluster purity when combining the highly reproducible component from %s group with its spatial counterpart in %s group: \n', groups(i).name, groups(i-1).name)];
                end
                tot_purity = opp(i).g(1).purity + opp(i).g(2).purity
                res_str = [res_str , num2str(tot_purity), '\n'];

            end
        end    
        
        %disp(res_str);
        
        % 3 Groups
        
        if (numGroups == 3)
            
            gcc(1).maxComp = groups(1).maxCompPath;
            gcc(1).compMaps(1).val = groups(2).compMaps;
            gcc(1).compMaps(2).val = groups(3).compMaps;

            gcc(2).maxComp = groups(2).maxCompPath;
            gcc(2).compMaps(1).val = groups(1).compMaps;
            gcc(2).compMaps(2).val = groups(3).compMaps;
            
            gcc(3).maxComp = groups(3).maxCompPath;
            gcc(3).compMaps(1).val = groups(1).compMaps;
            gcc(3).compMaps(2).val = groups(2).compMaps;

            for i = 1:numGroups
                
                [index path P R] = GetCorrCoeff3g(gcc(i), groups(i).ax, groups(i).ay, groups(i).az);
                gcc(i).index = index;
                
                switch i
                    
                    case 1
                        res_str = [res_str, sprintf('Component %d from %s had the highest spatial correlation (%f) with component (%s) from %s group and the highest spatial correlation (%f) with (%s) from %s group.\n ', groups(1).compIndex, groups(1).name, R(1).value, path(1).value, groups(2).name, R(2).value, path(2).value, groups(3).name)];
                    case 2
                        res_str = [res_str, sprintf('Component %d from %s had the highest spatial correlation (%f) with component (%s) from %s group and the highest spatial correlation (%f) with (%s) from %s group.\n ', groups(2).compIndex, groups(2).name, R(1).value, path(1).value, groups(1).name, R(2).value, path(2).value, groups(3).name)];
                    case 3
                        res_str = [res_str, sprintf('Component %d from %s had the highest spatial correlation (%f) with component (%s) from %s group and the highest spatial correlation (%f) with (%s) from %s group.\n ', groups(3).compIndex, groups(3).name, R(1).value, path(1).value, groups(1).name, R(2).value, path(2).value, groups(2).name)];

                end
            end

            % get x_vecs for these two
            
           for i = 1:numGroups % 2
               
                tvec = length(gcc(i).index);
                
                for j = 1:tvec
                    
                    [error, opp_vec] = SetupOpp_Vec(groups(i).resultFile, groups(i).melodicFiles, gcc(i).index(j).value, pathstr);
                    groups(i).opp_vec(j).val = opp_vec;
                    
                end

            end

            % Put through K Means
            for i = 1:numGroups
                
                if      (i == 1)
                    gsub(1).val = 2
                    gsub(2).val = 3
                elseif  (i == 2)
                    gsub(1).val = 1
                    gsub(2).val = 3
                elseif  (i == 3)
                    gsub(1).val = 1
                    gsub(2).val = 2
                end
                
                s_1(1) = numel(groups(i).melodicFiles);
                s_1(2) = numel(groups(gsub(1).val).melodicFiles);
                s_1(3) = numel(groups(gsub(2).val).melodicFiles);
                
                opp(i).vec = [groups(i).currcompvec; groups(i).opp_vec(1).val ; groups(i).opp_vec(2).val]; 
                opp(i).mean = [mean(groups(i).currcompvec); mean(groups(i).opp_vec(1).val); mean(groups(i).opp_vec(2).val)];
                
                g = KMeans(numGroups, s_1, opp(i).vec, opp(i).mean);
                
                opp(i).g = g;

                disp(opp(i).g(1).purity)
                disp(opp(i).g(2).purity)
                disp(opp(i).g(3).purity)    
                
                switch i
                    
                    case 1
                         res_str = [res_str, sprintf('Cluster purity when combining the highly reproducible component from %s group with its spatial counterpart in %s group and %s group: \n', groups(i).name, groups(i+1).name, groups(i+2).name)];
                     case 2
                         res_str = [res_str, sprintf('Cluster purity when combining the highly reproducible component from %s group with its spatial counterpart in %s group and %s group: \n', groups(i).name, groups(1).name, groups(3).name)];
                     case 3
                         res_str = [res_str, sprintf('Cluster purity when combining the highly reproducible component from %s group with its spatial counterpart in %s group and %s group: \n', groups(i).name, groups(1).name, groups(3).name)];
                end
                
                tot_purity = opp(i).g(1).purity + opp(i).g(2).purity + opp(i).g(3).purity
                res_str = [res_str , num2str(tot_purity), '\n'];
                
            end
            
        end    
        
        
        % End 3 Groups
        
        fileID = fopen(res_fn, open_res);
        fprintf(fileID, res_str);
        fclose(fileID);
        
        log = sprintf('%s\n%s %s\n', log, datestr(now), 'Ending Set Up Clustering');
        fileID = fopen(log_fn, open);
        fprintf(fileID, log);
        fclose(fileID);
        
catch e
    
    error = 1;
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'Main Error, Identifier: ', e.identifier)
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'Main Error, Message: ', e.message)
    log = strrep(log, '\', '\\');
    log = sprintf('%s\n%s %s\n', log, datestr(now), 'Set up clustering did not complete successfully');
    fileID = fopen(log_fn, open);
    fprintf(fileID, log);
    fclose(fileID);
    
end
        
        
        
        
        
        % %
        % %
        % % %
        % % %
        % % %
