function [error, groups] = SetupX_Vec(groups, pathstr)

dfpath = '/gpfs01/home/mas0047/TestData4';
formatOut = 'mm_dd_yy';
log_fn = ['gmedica_log_' datestr(now,formatOut) '.log'];
% soft = mfilename('fullpath');
% [pathstr,name,ext] = fileparts(soft);
NIFTI_PATH = sprintf('%s/%s', pathstr, 'NIfTI');
addpath(NIFTI_PATH);
log_loc = fullfile(pathstr, 'logs');

if exist(log_loc, 'dir') ~= 7
    mkdir(log_loc);
end

log_fn = fullfile(log_loc, log_fn);

if exist(log_fn, 'file') == 2
    open = 'w';
else
    open = 'a';
end

%
log = sprintf('%s %s', datestr(now), 'Setup X_vec Started');
log = sprintf('%s\n%s %s', log, datestr(now), 'Setting up initialization parameters');

fileID = fopen(log_fn, open);
fprintf(fileID, log);
fclose(fileID);
error = 0;

numGroups = length(groups);



try
    
    %
    for i = 1: numGroups   % should match the number of groups
            
            load(groups(i).resultFile);
           
            for j = 1: numel(groups(i).inds);
                
                curComp = groups(i).inds(j); 
                
                s = size(obj.result.foundComp(1,:));
                
                ICIndex = {};               %# Read the first element (1,1)
                
                for i1 = 1: s(2)               %# Loop from subject 1 thru s(1)
                    ICIndex = [ICIndex obj.result.foundComp(curComp, i1)];       %# Add the element to ICIndex
                end
                
                for i2 = 1: length(groups(i).melodicFiles)
                    % files have been added because they exist
                    
                    a = load_nii(groups(i).melodicFiles{1,i2});
                    ax=a.hdr.dime.dim(2);
                    ay=a.hdr.dime.dim(3);
                    az=a.hdr.dime.dim(4);
                    x = squeeze(a.img(:,:,:,ICIndex{i2}));
                    
                    if i2 == 1
                        
                        x_vec=reshape(x,1,ax*ay*az);
                        
                    else
                        
                        y_vec=reshape(x,1,ax*ay*az);
                        x_vec = [x_vec; y_vec];
                        
                    end
                    
                    
                    
                end
%                 
%                 size(x_vec);
                groups(i).ax = ax;
                groups(i).ay = ay;
                groups(i).az = az;
                groups(i).x_vec(j).ind = groups(i).inds(j);
                groups(i).x_vec(j).vec = x_vec; %x_vec;
                
                
            end
            
    end
    %
   
    
    log = sprintf('%s\n%s %s', log, datestr(now), 'Ending Set Up Clustering');
    fileID = fopen(log_fn, open);
    fprintf(fileID, log);
    fclose(fileID);
    
catch e
    
    error = 1;
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'Main Error, Identifier: ', e.identifier)
    log = sprintf('%s\n%s %s %s', log, datestr(now), 'Main Error, Message: ', e.message)
    log = strrep(log, '\', '\\');
    log = sprintf('%s\n%s %s', log, datestr(now), 'Set up clustering did not complete successfully');
    fileID = fopen(log_fn, open);
    fprintf(fileID, log);
    fclose(fileID);
    
end