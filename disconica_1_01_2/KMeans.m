function [ g ] = KMeans(numGroups, subjects, x_vec, mean)

[IDX, ctrs] = kmeans(x_vec, numGroups, 'start', mean);
IDX
count = 0;

    for i=1:numGroups
        g(i).positive = 0;
        g(i).negative = 0;
           
       if i == 1

           for j = 1:subjects(i)
               if IDX(j) == i
                   g(i).positive = g(i).positive + 1;

               else
                   g(i).negative = g(i).negative + 1;
               end
           end
           g(i).purity = g(i).positive/subjects(i);
       else
           
           if count == 0
            start = subjects(i-1) + 1;
           else
            start = count + 1;
           end
           
           if count == 0
            stop = subjects(i-1) + subjects(i);
           else
               stop = count + subjects(i);
           end
           count = stop;
           for j = start:stop
               if IDX(j) == i
                   g(i).positive = g(i).positive + 1;

               else
                   g(i).negative = g(i).negative + 1;
               end
           end
           g(i).purity = g(i).positive/subjects(i);

       end


    end


end
