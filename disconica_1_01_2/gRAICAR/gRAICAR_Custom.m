function varargout = gRAICAR_Custom(gRIP)
% Later: Send any parameters

%     handles.ncores  = str2double(getenv('NUMBER_OF_PROCESSORS'));
%     handles.maxcores = str2double(getenv('NUMBER_OF_PROCESSORS'));
% 
%     handles.ncores  = feature('numcores');
%     handles.maxcores = feature('numcores');
% end
% handles.savemovie = 1;
% handles.webreport = 1;
% handles.useRAICAR = 0;

varargout{1} = 1;
addpath(gRIP.core)
addpath(gRIP.fastICA)
addpath(gRIP.raicar)
addpath(gRIP.strings)
addpath(gRIP.workdir)

ncores = 2;

%maxcores = feature('numcores');%str2double(getenv('NUMBER_OF_PROCESSORS'));

% if ncores > maxcores
%     settings.ncores = maxcores;
% elseif ncores < 1
%     settings.ncores = 1;
% elseif maxcores > 1
%     settings.ncores = maxcores;
% else
%     settings.ncores = ncores;
% end
% 
% %settings.ncores = 1;
% 
% if maxcores > 1
%     settings.maxcores = maxcores;
% else
%     settings.maxcores = 4;
% end

% if ncores < maxcores 
%      settings.ncores = maxcores
% else
%      settings.ncores = 2
% end
% if maxcores > 19
%     settings.ncores = 10
% elseif maxcores > 10 & maxcores <= 19
%     settings.ncores = 5
% else
%     settings.ncores = 2
% end
settings.ncores = 2


settings.maxcores = 4

% 
settings.savemovie = 0;
settings.webreport = 0;
settings.useRAICAR = 0;

settings.workdir = gRIP.workdir;
settings.outDir = '/gR_Results';

settings.pathSbList = '/subjects.list';
settings.maskPath = '/mask.nii.gz';
settings.taskname = 'task';

settings.icaDir = '/filtered_func_data.ica';
settings.icaPrefix = 'melodic_IC.nii.gz';

% default settings
    if ~isfield(settings, 'displayThreshold')
        settings.displayThreshold = 1.5;
    end
    if ~isfield(settings, 'compPerPage')
        settings.compPerPage = 10;
    end
    if ~isfield(settings, 'reproThr')
        settings.reproThr = 0.5; % threshold of mean reproducibility in RAICAR, for selecting reproducibile ICs before gRAICAR.
    end

gRAICAR_step1(settings);

%adjust
% if ncores > handles.maxcores
%     handles.ncores = handles.maxcores;
% elseif ncores < 1
%     handles.ncores = 1;
% else
%     handles.ncores = round (ncores);
% end


% [pass, settings] = gRAICAR_check_settings (settings);
% if pass == 1
%     fn = sprintf ('gRAICAR_settings_%s_%s.mat', handles.taskname, datestr(now, 'mmm-dd.HH-MM'));
%     %uisave ('settings', fn);
% end


% --- Executes on button press in btn_load.
%function btn_load_Callback(hObject, eventdata, handles)
% hObject    handle to btn_load (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% load settings
% [filename, pathname] = uigetfile('*.mat', 'Select a gRAICAR setting file:');
% if filename ~= 0
%     load (fullfile(pathname, filename));
%     for nm = fieldnames(settings)'
%         fdnm = cell2mat (nm);
%         handles.(fdnm) = settings.(fdnm);
%     end
%     guidata(hObject, handles);
% 
%     % update ui
%     set(handles.edit_workdir,   'string', handles.workdir, 'ForegroundColor', [0 0 0], 'ButtonDownFcn', [], 'Enable', 'on');
%     set(handles.edit_outputDir, 'string', handles.outdir, 'ForegroundColor', [0 0 0], 'ButtonDownFcn', [], 'Enable', 'on');
%     set(handles.edit_subjList,  'string', handles.subjlist, 'ForegroundColor', [0 0 0], 'ButtonDownFcn', [], 'Enable', 'on');
%     set(handles.edit_taskName,  'string', handles.taskname, 'ForegroundColor', [0 0 0], 'ButtonDownFcn', [], 'Enable', 'on');
%     if handles.useRAICAR == 0
%         set(handles.checkbox_RAICAR, 'value', 0);
%         set(handles.text5, 'string', 'First ICA File');
%         set(handles.edit_ICAdir, 'string', handles.icapath, 'ForegroundColor', [0 0 0], 'ButtonDownFcn', [], 'Enable', 'on');
%     else
%         set(handles.checkbox_RAICAR, 'value', 1);
%         set(handles.text5, 'string', 'First fMRI file')
%         set(handles.edit_ICAdir, 'string', handles.fmripath, 'ForegroundColor', [0 0 0], 'ButtonDownFcn', [], 'Enable', 'on');
%     end
%     set(handles.edit_maskPath,  'string', handles.maskpath, 'ForegroundColor', [0 0 0], 'ButtonDownFcn', [], 'Enable', 'on');
%     set(handles.edit_numCores,  'string', handles.ncores, 'ForegroundColor', [0 0 0], 'ButtonDownFcn', [], 'Enable', 'on');
%     set(handles.checkbox_saveMovie, 'value', handles.savemovie, 'ForegroundColor', [0 0 0], 'ButtonDownFcn', [], 'Enable', 'on');
%     set(handles.checkbox_report, 'value', handles.webreport, 'ForegroundColor', [0 0 0], 'ButtonDownFcn', [], 'Enable', 'on');
% end
% 
% % --- Executes on button press in btn_run.
% function btn_run_Callback(hObject, eventdata, handles)
% % hObject    handle to btn_run (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% [pass, settings] = gRAICAR_check_settings (handles);
% if pass == 1
%     % start step 1
%     set(hObject, 'Enable', 'off');
%     set(handles.text_status, 'Visible', 'on', 'String', 'Setting up gRAICAR analysis...', 'Foregroundcolor', [0,0,1]);drawnow;
%     [status, exeption] = gRAICAR_step1(settings);
%     if status == 0  % check error
%         set(hObject, 'Enable', 'on');
%         set(handles.text_status, 'Visible', 'on', 'String', 'Error in gRAICAR setup', 'Foregroundcolor', [1,0,0]);drawnow;
%         rethrow (exeption);
%     end
%     
%     % start step 2
%     set(handles.text_status, 'Visible', 'on', 'String', 'Computing similarity matrix...', 'Foregroundcolor', [0,0,1]);drawnow;
%     [status, exeption] = gRAICAR_step2(settings);
%     if status == 0
%         set(hObject, 'Enable', 'on');
%         set(handles.text_status, 'Visible', 'on', 'String', 'Error in distributed computing', 'Foregroundcolor', [1,0,0]);drawnow;
%         rethrow (exeption);
%     end
%     
%     % start step 3
%     set(handles.text_status, 'Visible', 'on', 'String', 'Post processing...', 'Foregroundcolor', [0,0,1]); drawnow;
%     [status, exeption] = gRAICAR_step3(settings);
%     if status == 0
%         set(hObject, 'Enable', 'on');
%         set(handles.text_status, 'Visible', 'on', 'String', 'Error in post processing', 'Foregroundcolor', [1,0,0]);drawnow;
%         rethrow (exeption);
%     end
%     
%     % when finished
%     set(hObject, 'Enable', 'on');
%     set(handles.text_status, 'Visible', 'on', 'String', 'gRAICAR done', 'Foregroundcolor', [0,0,1]);drawnow;    
% end
% 
% 
% % --- If Enable == 'on', executes on mouse press in 5 pixel border.
% % --- Otherwise, executes on mouse press in 5 pixel border or over edit_workdir.
% function edit_workdir_ButtonDownFcn(hObject, eventdata, handles)
% % hObject    handle to edit_workdir (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% set(hObject, 'string', []);
% set(hObject, 'Enable', 'on');
% set(hObject, 'ForegroundColor', [0 0 0]);
% uicontrol(hObject);
% 
% 
% % --- If Enable == 'on', executes on mouse press in 5 pixel border.
% % --- Otherwise, executes on mouse press in 5 pixel border or over edit_outputDir.
% function edit_outputDir_ButtonDownFcn(hObject, eventdata, handles)
% % hObject    handle to edit_outputDir (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% set(hObject, 'string', []);
% set(hObject, 'Enable', 'on');
% set(hObject, 'ForegroundColor', [0 0 0]);
% uicontrol(hObject);
% 
% % --- If Enable == 'on', executes on mouse press in 5 pixel border.
% % --- Otherwise, executes on mouse press in 5 pixel border or over edit_subjList.
% function edit_subjList_ButtonDownFcn(hObject, eventdata, handles)
% % hObject    handle to edit_subjList (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% set(hObject, 'string', []);
% set(hObject, 'Enable', 'on');
% set(hObject, 'ForegroundColor', [0 0 0]);
% uicontrol(hObject);
% 
% % --- If Enable == 'on', executes on mouse press in 5 pixel border.
% % --- Otherwise, executes on mouse press in 5 pixel border or over edit_maskPath.
% function edit_maskPath_ButtonDownFcn(hObject, eventdata, handles)
% % hObject    handle to edit_maskPath (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% set(hObject, 'string', []);
% set(hObject, 'Enable', 'on');
% set(hObject, 'ForegroundColor', [0 0 0]);
% uicontrol(hObject);
% 
% % --- If Enable == 'on', executes on mouse press in 5 pixel border.
% % --- Otherwise, executes on mouse press in 5 pixel border or over edit_taskName.
% function edit_taskName_ButtonDownFcn(hObject, eventdata, handles)
% % hObject    handle to edit_taskName (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% set(hObject, 'string', []);
% set(hObject, 'Enable', 'on');
% set(hObject, 'ForegroundColor', [0 0 0]);
% set(hObject, 'ButtonDownFcn', []); % disable the buttondown event in the corresponding edit box
% uicontrol(hObject);
% 
% % --- If Enable == 'on', executes on mouse press in 5 pixel border.
% % --- Otherwise, executes on mouse press in 5 pixel border or over edit_ICAdir.
% function edit_ICAdir_ButtonDownFcn(hObject, eventdata, handles)
% % hObject    handle to edit_ICAdir (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% set(hObject, 'string', []);
% set(hObject, 'Enable', 'on');
% set(hObject, 'ForegroundColor', [0 0 0]);
% uicontrol(hObject);
% 
% % --- If Enable == 'on', executes on mouse press in 5 pixel border.
% % --- Otherwise, executes on mouse press in 5 pixel border or over edit_ICAprefix.
% function edit_ICAprefix_ButtonDownFcn(hObject, eventdata, handles)
% % hObject    handle to edit_ICAprefix (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% set(hObject, 'string', []);
% set(hObject, 'Enable', 'on');
% set(hObject, 'ForegroundColor', [0 0 0]);
% uicontrol(hObject);
% 
% % --- If Enable == 'on', executes on mouse press in 5 pixel border.
% % --- Otherwise, executes on mouse press in 5 pixel border or over edit_numCores.
% function edit_numCores_ButtonDownFcn(hObject, eventdata, handles)
% % hObject    handle to edit_numCores (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% set(hObject, 'string', []);
% set(hObject, 'Enable', 'on');
% set(hObject, 'ForegroundColor', [0 0 0]);
% uicontrol(hObject);
% 
% 
% % --- Executes on button press in btn_ICAfile.
% function btn_ICAfile_Callback(hObject, eventdata, handles)
% % hObject    handle to btn_ICAfile (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% if handles.useRAICAR == 0
%     [fn, fpth] = uigetfile('*.*', 'Select an ICA result file (.nii or .nii.gz):');
%     if fn == 0
%         handles.icapath = '';
%     else
%         handles.icapath = fullfile (fpth, fn);
%     end
%     set(handles.edit_ICAdir, 'String', handles.icapath);
% else
%     [fn, fpth] = uigetfile('*.*', 'Select an fMRI file (.nii or .nii.gz):');
%     if fn == 0
%         handles.fmripath = '';
%     else
%         handles.fmripath = fullfile (fpth, fn);
%     end
%     set(handles.edit_ICAdir, 'String', handles.fmripath);
% end
% 
% set(handles.edit_ICAdir, 'Enable', 'on');
% set(handles.edit_ICAdir, 'ForegroundColor', [0 0 0]);
% set(handles.edit_ICAdir, 'ButtonDownFcn', []); % disable the buttondown event in the corresponding edit box
% 
% guidata(hObject, handles);
% 
% % --- Executes on button press in checkbox_saveMovie.
% function checkbox_saveMovie_Callback(hObject, eventdata, handles)
% % hObject    handle to checkbox_saveMovie (see GCBO) 
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% % Hint: get(hObject,'Value') returns toggle state of checkbox_saveMovie
% handles.savemovie = get(hObject, 'Value');
% guidata(hObject, handles);
% 
% % --- Executes on button press in checkbox_report.
% function checkbox_report_Callback(hObject, eventdata, handles)
% % hObject    handle to checkbox_report (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% % Hint: get(hObject,'Value') returns toggle state of checkbox_report
% handles.webreport = get(hObject, 'Value');
% guidata(hObject, handles);
% 
% 
% % --- Executes on button press in checkbox_RAICAR.
% function checkbox_RAICAR_Callback(hObject, eventdata, handles)
% % hObject    handle to checkbox_RAICAR (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% % Hint: get(hObject,'Value') returns toggle state of checkbox_RAICAR
% handles.useRAICAR = get(hObject, 'Value');
% if handles.useRAICAR == 1
%     set (handles.text5, 'string', 'First fMRI file');
%     if isempty (handles.fmripath)
%         set (handles.edit_ICAdir, 'string', 'processed fMRI file for the first subject', 'ForegroundColor', [1 0 0]);
%     else
%         set (handles.edit_ICAdir, 'string', handles.fmripath, 'ForegroundColor', [0 0 0]);
%     end    
% else
%     set (handles.text5, 'string', 'First ICA file');
%     if isempty (handles.icapath)
%         set (handles.edit_ICAdir, 'string', 'ICA result file for the first subject', 'ForegroundColor', [1 0 0]);
%     else
%         set (handles.edit_ICAdir, 'string', handles.icapath, 'ForegroundColor', [0 0 0]);
%     end
% end
% guidata(hObject, handles);
