#!/bin/bash
#asking for 1 node and 16 processor for 20 hours
#you already configured parallel computing toolbox

#PBS -l nodes=1:ppn=16,walltime=20:00:00

#following 2 lines ensures you'll be notified by email after job completion
#PBS -M sample@auburn.edu
#PBS -m e

#PBS -o default.out
#PBS -e errorfile
#invoking matlab, <sourcefile > writing output file
module load fsl/6.0
source /tools/fsl-6.0/etc/fslconf/fsl.sh
/tools/licensed/matlab-2017a/bin/matlab -nodisplay -nosplash <"/home/disconica_1_01_2/disconica_batch.m"> "/home/disconica_1_01_2/disconica_batch.out"

