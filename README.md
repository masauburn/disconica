Auburn University MRI Research Center

DisConICA Version 1.01.1 
DisConICA Version 1.01.2
Discover and Confirm Discover Confirm Independent Component Analysis
Last Updated: 02/03/2019
Reference: Syed, M.A., Yang, Z., Rangaprakash, D. et al. Neuroinform (2019). https://doi.org/10.1007/s12021-019-09422-1

General Instructions:
For Interactive Mode:
1. Download/ Clone disconica_1_01_1 software directory - master branch 
2. Download TestData.zip
3. Unzip TestData.zip
4. Follow instructions in the instructions manual in PDF format: 'Instructions for Processing with DisConICA_1_01_1'

For High Performance Computing (HPC)/ Batch Mode: 
1. Download/ Clone disconica_1_01_2 software directory - master branch 
2. Download TestData.zip
3. Unzip TestData.zip
4. Follow instructions in the instructions manual in PDF format: 'Instructions for Processing with DisConICA_1_01_2'

Current Version data input:
.dcm
.img/.hdr pair
